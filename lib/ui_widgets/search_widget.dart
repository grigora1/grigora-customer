import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/views/category_single/category_single_screen.dart';
import 'package:grigora/views/home/filter_screen.dart';
import 'package:nb_utils/nb_utils.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Flexible(
        child: Container(
          // height: 60,
          // padding: kPaddingAllSmall,
          decoration: BoxDecoration(
              color: kColorWhite,
              border: Border.all(color: Color(0xFFF2F2F2)),
              borderRadius: kBorderRadius),
          child: Row(
            children: [
              Icon(
                EvaIcons.searchOutline,
                color: Color(0xFFA9A9A9),
              ).paddingRight(kSpacingSmall),
              FormItem(
                placeholder: 'Search for food',
                withNoBorder: true,
              ).expand(),
              SvgPicture.asset(kImagesBarcodeScan)
            ],
          ).paddingSymmetric(horizontal: kSpacingSmall, vertical: 5),
        ),
      ),
      SizedBox(
        width: 4,
      ),
      GestureDetector(
          onTap: () => UiUtils.goToWidget(
                FilterScreen(),
              ),
          child: SvgPicture.asset(kImagesFilterIcon))
    ]);
  }
}
