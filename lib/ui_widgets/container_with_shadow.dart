import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';

class ContainerWithShadow extends StatelessWidget {
  final Widget child;
  final double? height;
  final EdgeInsetsGeometry? padding;
  const ContainerWithShadow(
      {Key? key, required this.child, this.padding, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: padding ?? kPaddingAllSmall,
      decoration: BoxDecoration(
          color: kColorWhite,
          boxShadow: [kBoxShadow(kColorGrey)],
          borderRadius: kBorderRadius),
      child: child,
    );
  }
}
