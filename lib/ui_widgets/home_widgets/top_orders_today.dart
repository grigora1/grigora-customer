import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:nb_utils/nb_utils.dart';

class TopOrdersToday extends StatelessWidget {
  const TopOrdersToday({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<RestaurantWidgetModel> _items = [
      RestaurantWidgetModel(
          title: 'Item7 Grill',
          time: '32 Mins',
          rating: '4.8',
          image:
              'https://marketingedge.com.ng/wp-content/uploads/2021/03/Burger-King-860x500.png'),
      RestaurantWidgetModel(
          title: 'Crelo Speaker',
          time: '32 Mins',
          rating: '4.7',
          image:
              'https://www.playwinterpark.com/sites/default/files/styles/listing_node_full/public/mmg_lfef_images/mcdonalds-of-winter-park-2801-2940.gif?itok=7jNvEHFy'),
      RestaurantWidgetModel(
          title: 'Item7 Grill',
          time: '32 Mins',
          image:
              'https://www.businesslist.com.ng/img/ng/e/1605806387-61-shoprite.png'),
      RestaurantWidgetModel(
          title: 'Matrite',
          time: '32 Mins',
          image:
              'https://www.businesslist.com.ng/img/ng/e/1605806387-61-shoprite.png'),
    ];
    return GrigoraSectionWidget(
      title: 'Top Orders Today',
      showAllAction: () => print('do show all'),
      child: GrigoraHorizontalScroll<RestaurantWidgetModel>(
          builder: (position, model) => TopOrdersCard(
              width: 200,
              image: model.image,
              title: model.title,
              rating: model.rating,
              distance: model.distance,
              onTap: () => Navigator.pushNamed(context, kRouteRestaurantSingle,
                  arguments: _items[position]),),
          items: _items),
    );
  }
}
