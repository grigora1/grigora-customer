import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/models/args.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:grigora/views/category_single/category_single_screen.dart';

class _PurchaseAnItemModel {
  final String image, title;
  final int places;
  _PurchaseAnItemModel(
      {required this.image, required this.title, required this.places});
}

class PurchaseAnItemWidget extends StatelessWidget {
  const PurchaseAnItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_PurchaseAnItemModel> _items = [
      _PurchaseAnItemModel(
          title: 'Food',
          places: 600,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_chfkvn.png'),
      _PurchaseAnItemModel(
          title: 'Grocerys',
          places: 100,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_1_w6reov.png'),
      _PurchaseAnItemModel(
          title: 'Drugs',
          places: 200,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_3_aq5mjd.png'),
      _PurchaseAnItemModel(
          title: 'Furnitures',
          places: 30,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_2_uqyi2t.png'),
      _PurchaseAnItemModel(
          title: 'Electronics',
          places: 600,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_4_ailkxw.png'),
      _PurchaseAnItemModel(
          title: 'Toys',
          places: 60,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_5_muwdg1.png'),
      _PurchaseAnItemModel(
          title: 'Gift Shop',
          places: 200,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_6_w4uzoj.png'),
      _PurchaseAnItemModel(
          title: 'Fashion',
          places: 400,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627269959/grigora/Rectangle_221_7_ifsdhc.png'),
    ];
    return GrigoraSectionWidget(
      title: 'Purchase an Item',
      child: Wrap(
        spacing: kSpacingMedium,
        runSpacing: kSpacingMedium,
        children: _items
            .asMap()
            .map((int position, _PurchaseAnItemModel model) => MapEntry(
                position,
                _PurchaseAnItem(
                  image: model.image,
                  title: model.title,
                  places: model.places,
                  onTap: () => UiUtils.goToWidget(CategorySingleScreen(
                    item: ArgsCategory(title: model.title),
                  )),
                ).withWidth(kWidthFull(context) / 2.4)))
            .values
            .toList(),
      ).paddingSymmetric(horizontal: kSpacingMedium),
    );
  }
}

class _PurchaseAnItem extends StatelessWidget {
  final String image, title;
  final int places;
  final Function()? onTap;
  const _PurchaseAnItem(
      {Key? key,
      required this.image,
      required this.title,
      required this.places,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWithShadow(
      child: Row(
        children: [
          GrigoraNetworkImage(image).paddingRight(kSpacingSmall),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle(
                title,
                textSize: kFontSizeMedium,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ).paddingBottom(kSpacingSmall),
              GrigoraText('$places Places')
            ],
          ))
        ],
      ),
    ).onTap(onTap);
  }
}
