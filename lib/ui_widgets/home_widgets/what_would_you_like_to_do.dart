import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';

class _ItemModel {
  final String image, title;
  final String? description;
  _ItemModel({required this.image, required this.title, this.description});
}

class WhatWouldYouLikeToDo extends StatelessWidget {
  const WhatWouldYouLikeToDo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ItemModel> _items = [
      _ItemModel(
          title: 'Request a Delivery',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627282725/grigora/Group_1_j7jmwg.png'),
      _ItemModel(
          title: 'Create a Custom Order',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627282722/grigora/Group_641_htn3dz.png'),
    ];
    return GrigoraSectionWidget(
        title: 'What would you like to do',
        child: Wrap(
          spacing: kSpacingMedium,
          runSpacing: kSpacingMedium,
          children: _items
              .asMap()
              .map((int position, _ItemModel model) => MapEntry(
                  position,
                  _Item(
                    image: model.image,
                    title: model.title,
                    description: model.description,
                    onTap: () => Navigator.pushNamed(
                        context, kRouteRestaurantSingle,
                        arguments: _items[position]),
                  ).withWidth(kWidthFull(context) / 2.4)))
              .values
              .toList(),
        ).paddingSymmetric(horizontal: kSpacingMedium));
  }
}

class _Item extends StatelessWidget {
  final String image, title;
  final String? description;
  final Function()? onTap;
  const _Item(
      {Key? key,
      required this.image,
      required this.title,
      this.description,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWithShadow(
      padding: kPaddingAllMedium,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraNetworkImage(image).paddingBottom(kSpacingSmall),
          GrigoraTextTitle(
            title,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ).paddingBottom(kSpacingSmall),
          GrigoraText(description ?? 'Purchase an item from any store')
              .paddingBottom(kSpacingSmall),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(EvaIcons.arrowForward),
            ],
          )
        ],
      ),
    );
  }
}
