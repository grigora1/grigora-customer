import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';

class FABBottomAppBarItem {
  final String imageAsset;
  final String title;

  FABBottomAppBarItem({required this.imageAsset, required this.title});
}

class FABBottomAppBar extends StatefulWidget {
  FABBottomAppBar(
      {required this.items,
      required this.centerItemText,
      this.height: 60.0,
      this.iconSize: 24.0,
      required this.backgroundColor,
      required this.color,
      required this.selectedColor,
      required this.onTabSelected});
  final List<FABBottomAppBarItem> items;
  final String centerItemText;
  final double height;
  final double iconSize;
  final Color backgroundColor;
  final Color color;
  final Color selectedColor;
  final ValueChanged<int> onTabSelected;

  @override
  State<StatefulWidget> createState() => FABBottomAppBarState();
}

class FABBottomAppBarState extends State<FABBottomAppBar> {
  int _selectedIndex = 0;

  _updateIndex(int index) {
    widget.onTabSelected(index);
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = List.generate(widget.items.length, (int index) {
      return _buildTabItem(
        item: widget.items[index],
        index: index,
        onPressed: _updateIndex,
      );
    });
    // items.insert(items.length >> 1, _buildMiddleTabItem());

    return BottomAppBar(
      elevation: 20,
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: items,
        ),
      ),
      color: widget.backgroundColor,
    );
  }

  // Widget _buildMiddleTabItem() {
  //   return Expanded(
  //     child: SizedBox(
  //       height: widget.height,
  //       child: Column(
  //         mainAxisSize: MainAxisSize.min,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: <Widget>[
  //           SizedBox(height: widget.iconSize),
  //           Text(
  //             widget.centerItemText ?? '',
  //             style: TextStyle(color: widget.color),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  Widget _buildTabItem({
    required FABBottomAppBarItem item,
    required int index,
    required ValueChanged<int> onPressed,
  }) {
    // String asset = _selectedIndex == index ? widget.selectedA : widget.color;
    return Expanded(
      child: InkWell(
        splashColor: Colors.transparent,
        focusColor: Colors.transparent,
        hoverColor: Colors.transparent,
        // overlayColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () => onPressed(index),
        child: Container(
          height: widget.height,
          child: Material(
            type: MaterialType.transparency,
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _selectedIndex == index
                      ? Container(
                          height: widget.iconSize,
                          child: SvgPicture.asset(
                            item.imageAsset,
                            color: kColorPrimaryRed,
                            height: widget.iconSize,
                          ),
                        )
                      : Container(
                          height: widget.iconSize,
                          child: SvgPicture.asset(
                            item.imageAsset,
                            color: kColorGenericGrey,
                            height: widget.iconSize,
                          ),
                        ),
                  GrigoraText(
                    item.title,
                    textColor: _selectedIndex == index
                        ? kColorPrimaryRed
                        : kColorGenericGrey,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
