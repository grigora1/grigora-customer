import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';

class _ItemModel extends HorizontalScrollModel {
  final String image;
  _ItemModel({required this.image});
}

class SliderWidget extends StatelessWidget {
  const SliderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ItemModel> _items = [
      _ItemModel(
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627288540/grigora/Frame_896_nzt68j.png'),
      _ItemModel(
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627288540/grigora/Frame_896_nzt68j.png'),
    ];
    return GrigoraSectionWidget(
      title: 'Slider',
      hideTitle: true,
      child: GrigoraHorizontalScroll<_ItemModel>(
          items: _items,
          builder: (position, model) => _Widget(
                image: model.image,
                onTap: () => print('do nothing'),
              ).paddingRight(kSpacingMedium)),
    );
  }
}

class _Widget extends StatelessWidget {
  final String image;
  final Function()? onTap;
  const _Widget({Key? key, required this.image, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GrigoraNetworkImage(image).paddingRight(kSpacingSmall);
  }
}
