import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/models/args.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/category_single/category_single_screen.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';

class _ItemModel extends HorizontalScrollModel {
  final String image, title, time;
  _ItemModel({required this.image, required this.title, required this.time});
}

class OrderAgain extends StatelessWidget {
  const OrderAgain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ItemModel> _items = [
      _ItemModel(
          title: 'Meat (Steak)',
          time: '2 days ago',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627284748/grigora/Rectangle_221_8_szao8b.png'),
      _ItemModel(
          title: 'Chilli lzzz',
          time: '2 days ago',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627284739/grigora/Rectangle_221_9_xyrwr7.png'),
    ];
    return GrigoraSectionWidget(
      title: 'Order Again',
      showAllAction: () => UiUtils.goToWidget(CategorySingleScreen(
        item: ArgsCategory(title: 'Restaurant'),
      )),
      child: GrigoraHorizontalScroll<_ItemModel>(
          items: _items,
          builder: (position, model) => _Widget(
                image: model.image,
                title: model.title,
                time: model.time,
                // onTap: () => UiUtils.goToWidget(RestaurantSingleScreen(item: _items[position])),
              )
                  .paddingRight(kSpacingMedium)
                  .paddingTop(kSpacingSmall)
                  .paddingBottom(kSpacingMedium)),
    );
  }
}

class _Widget extends StatelessWidget {
  final String image, title, time;
  final Function()? onTap;
  const _Widget(
      {Key? key,
      required this.image,
      required this.title,
      required this.time,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWithShadow(
      child: Row(
        children: [
          GrigoraNetworkImage(image).paddingRight(kSpacingSmall),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GrigoraTextTitle(
                title,
                textSize: kFontSizeMedium,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ).paddingBottom(kSpacingSmall),
              GrigoraText(time)
            ],
          ).paddingRight(kSpacingMedium)
        ],
      ),
    );
  }
}
