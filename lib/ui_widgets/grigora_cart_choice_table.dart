import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/base_model.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/quantity_selector_widget.dart';

class GrigoraCartChoiceTableVM extends BaseModel {
  double _total = 1;
  double get total => _total;
  set total(double value) {
    _total = value;
    notifyListeners();
  }
}

class GrigoraCartChoiceTableModel {
  final String title;
  final double price;
  GrigoraCartChoiceTableModel({required this.title, required this.price});
}

class GrigoraCartChoiceTable extends StatelessWidget {
  final List<GrigoraCartChoiceTableModel> tbody;
  final List<String> thead;
  const GrigoraCartChoiceTable(
      {Key? key, required this.thead, required this.tbody})
      : super(key: key);

  List<Widget> _showBodyWidget(GrigoraCartChoiceTableModel rowData) {
    return [
      GrigoraText(
        rowData.title,
      ).paddingSymmetric(vertical: kSpacingSmall),
      GrigoraText(
        '${rowData.price}(NGN)',
        textAlign: TextAlign.end,
      ).paddingSymmetric(vertical: kSpacingSmall),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          QuantitySelectorWidget(
            callback: (val) => print(val),
            subtractBgColor: kColorDarkGrey.withOpacity(0.1),
            isSmallSize: true,
          ).paddingSymmetric(vertical: kSpacingSmall),
        ],
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<GrigoraCartChoiceTableVM>(
        model: GrigoraCartChoiceTableVM(),
        builder: (context, model, child) {
          return Table(
            columnWidths: const <int, TableColumnWidth>{
              0: FlexColumnWidth(),
              1: FlexColumnWidth(),
              2: FlexColumnWidth(),
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            children: <TableRow>[
              // table_head
              TableRow(
                  children: UiUtils.grigoraDynamicWidget(
                      items: thead,
                      builder: (position) => GrigoraTextTitle(
                            thead[position],
                            textSize: kFontSizeMedium,
                            textAlign:
                                position != 0 ? TextAlign.end : TextAlign.start,
                          ))),
              // table_body
              for (GrigoraCartChoiceTableModel rowData in tbody)
                TableRow(
                    children: _showBodyWidget(
                  rowData,
                )),
            ],
          );
        });
  }
}
