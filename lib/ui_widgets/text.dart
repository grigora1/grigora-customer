import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';

class GrigoraText extends StatelessWidget {
  final String text;
  final TextAlign? textAlign;
  final double? textSize;
  final Color? textColor;
  final TextOverflow? overflow;
  final double? height;
  final int? maxLines;
  final bool? isBoldFont;
  final bool? uppercase;
  final bool isLineThrough, isItalic;
  final double? letterSpacing;

  GrigoraText(this.text,
      {this.textAlign = TextAlign.left,
      this.textSize,
      this.textColor,
      this.maxLines,
      this.overflow,
      this.isLineThrough = false,
      this.letterSpacing,
      this.isItalic = false,
      this.uppercase = false,
      this.height,
      this.isBoldFont = false});

  @override
  Widget build(BuildContext context) {
    return Text(
      // this not clean_code but we'll name it like that
      text is String
          ? uppercase!
              ? text.toUpperCase()
              : text
          : 'no_text',
      // uppercase ? text.toUpperCase() : text,
      textAlign: textAlign ?? TextAlign.center,
      overflow: overflow ?? TextOverflow.visible,
      maxLines: maxLines ?? 100,
      style: new TextStyle(
        fontWeight: isBoldFont! ? FontWeight.bold : FontWeight.normal,
        letterSpacing: letterSpacing ?? 0,
        color: textColor ?? kColorPrimaryText,
        fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
        decoration:
            isLineThrough ? TextDecoration.lineThrough : TextDecoration.none,
        fontFamily: kFontFamilyRegular,
        // fontFamily: isBoldFont! ? kFontFamilyRegular : kFontFamilyLight,
        fontSize: textSize ?? kFontSizeNormal,
        height: height ?? 1.2,
      ),
    );
  }
}

class GrigoraTextTitle extends StatelessWidget {
  final String text;
  final Color? textColor;
  final TextAlign? textAlign;
  final int? maxLines;
  final TextOverflow? overflow;
  final bool isLightFont, isUpperCase;
  final double? textSize, letterSpacing;
  GrigoraTextTitle(this.text,
      {this.textColor,
      this.textSize,
      this.maxLines,
      this.overflow,
      this.isLightFont = false,
      this.isUpperCase = false,
      this.letterSpacing,
      this.textAlign = TextAlign.left});
  @override
  Widget build(BuildContext context) {
    return GrigoraText(
      text,
      uppercase: isUpperCase,
      maxLines: maxLines,
      overflow: overflow,
      isBoldFont: isLightFont ? false : true,
      textSize: textSize ?? kFontSizeLarge,
      letterSpacing: letterSpacing ?? 1,
      textColor: textColor ?? kColorBlack,
      textAlign: textAlign,
    );
  }
}
