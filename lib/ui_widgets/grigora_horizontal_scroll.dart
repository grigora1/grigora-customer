import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';

class HorizontalScrollModel {}

class GrigoraHorizontalScroll<T extends HorizontalScrollModel>
    extends StatelessWidget {
  final List<T> items;
  final Widget Function(int position, T model) builder;
  const GrigoraHorizontalScroll(
      {Key? key, required this.builder, required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.only(left: kMediumPadding),
            child: Row(
              children: items
                  .asMap()
                  .map((int position, T model) =>
                      MapEntry(position, builder(position, model)))
                  .values
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }
}
