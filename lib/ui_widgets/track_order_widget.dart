import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/views/track_order_single/track_order_single_screen.dart';

class TabOrderSingleWidget extends StatelessWidget {
  final bool hideRightInfo;
  const TabOrderSingleWidget({Key? key, this.hideRightInfo = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kColorWhite,
          border: Border(left: BorderSide(color: kColorPrimary, width: 5))),
      child: Row(
        children: [
          UiUtils.grigoraNetworkImageWithContainer(
                  'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627464677/grigora/Group_4045_yokqoq.png',
                  width: 50,
                  height: 50)
              .paddingAll(kSpacingSmall),
          Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                GrigoraTextTitle(
                  'Chicken Republic',
                  textSize: kFontSizeMedium,
                ),
                GrigoraText(
                  'Order ID: Crf18464'.toUpperCase(),
                  textSize: kFontSizeSmall,
                ).paddingTop(5),
                GrigoraText(
                  'Item Price: N120,000',
                ).paddingTop(5),
              ])).expand(),
          Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: kColorPrimary),
                    borderRadius: kBorderRadius),
                child: GrigoraTextTitle(
                  'View Details',
                  textSize: kFontSizeSmall,
                  textColor: kColorPrimary,
                ).paddingSymmetric(vertical: 5, horizontal: 5),
              ),
              GrigoraText(
                'Pending',
                textColor: kColorGold,
              ).paddingTop(kSpacingSmall)
            ],
          ).visible(hideRightInfo)
        ],
      ),
    ).onTap(() => UiUtils.goToWidget(TrackOrderSingleScreen()));
  }
}
