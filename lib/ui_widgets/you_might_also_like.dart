import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/models/args.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/views/category_single/category_single_screen.dart';

class _Model {
  final String image, title;
  final int places;
  _Model({required this.image, required this.title, required this.places});
}

class YouMightAlsoLike extends StatelessWidget {
  const YouMightAlsoLike({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_Model> _items = [
      _Model(
          title: 'Chivita',
          places: 1000,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627362929/grigora/Rectangle_13_pu3mdi.png'),
      _Model(
          title: 'Low Sugar Coda',
          places: 1000,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627362927/grigora/Rectangle_13_1_eiueat.png'),
      _Model(
          title: 'Cocacola',
          places: 1000,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627362924/grigora/Rectangle_13_2_uyousu.png'),
      _Model(
          title: 'Paracetamol Capsule',
          places: 1000,
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627362915/grigora/Rectangle_13_3_okfwje.png'),
    ];
    return Wrap(
      spacing: kSpacingMedium,
      runSpacing: kSpacingMedium,
      children: _items
          .asMap()
          .map((int position, _Model model) => MapEntry(
              position,
              _ItemWidget(
                image: model.image,
                title: model.title,
                places: model.places,
                onTap: () => UiUtils.goToWidget(CategorySingleScreen(
                  item: ArgsCategory(title: model.title),
                )),
              ).withWidth(kWidthFull(context) / 2.4)))
          .values
          .toList(),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final String image, title;
  final int places;
  final Function()? onTap;
  const _ItemWidget(
      {Key? key,
      required this.image,
      required this.title,
      required this.places,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWithShadow(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GrigoraNetworkImage(image).paddingBottom(kSpacingSmall),
          GrigoraText(
            title,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ).paddingBottom(5),
          GrigoraTextTitle(
            'N$places /Pc',
            textSize: kFontSizeMedium,
          )
        ],
      ),
    ).onTap(onTap);
  }
}
