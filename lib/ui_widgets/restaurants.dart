import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class RestaurantWidgetModel extends HorizontalScrollModel {
  final String title, time;
  final String distance;
  final bool open;
  final String image, category, deliveryTime, address, discount, rating, profilePic, workingHours;
  RestaurantWidgetModel(
      {required this.title,
      this.image = kImagesRemotePlaceholder,
      this.deliveryTime = 'Averagely 25mins',
      this.category = 'Fast Food',
      this.rating = 'Good (142 ratings)',
      this.address = 'No5, st Gogery Rd. beside Gst Gogery Rd. beside G',
      this.discount = '50% on purchase above N5000',
      required this.time,
      this.open = false,
      this.distance = '312m',
      this.profilePic = 'https://cdn.iconscout.com/icon/free/png-256/mcdonalds-3384870-2822951.png',
      this.workingHours = '8:00am - 8:00pm (12 hours)'});
}

class OffersWidgetModel {
  final String title, time;
  final String distance;
  final bool open;
  final double rating;

  final String image, category, deliveryTime, address, discount, profilePic, workingHours, minOrder;
  OffersWidgetModel(
      {required this.title,
      this.image = kImagesRemotePlaceholder,
      this.deliveryTime = 'Averagely 25mins',
      this.category = 'Fast Food',
      this.rating = 4.8,
      this.address = 'No5, st Gogery Rd. beside Gst Gogery Rd. beside G',
      this.discount = '50% on purchase above N5000',
      required this.time,
      this.open = false,
      this.distance = '312m',
      this.profilePic = 'https://cdn.iconscout.com/icon/free/png-256/mcdonalds-3384870-2822951.png',
      this.workingHours = '8:00am - 8:00pm (12 hours)',
      this.minOrder = 'Min Order: 5000'});
}

class PopularRestaurantItem extends StatelessWidget {
  final String title, time, subtitle;
  final String image;
  final double? width;
  final Function()? onTap;
  final String rating;
  final String distance;
  const PopularRestaurantItem(
      {Key? key,
      required this.title,
      required this.subtitle,
      this.onTap,
      required this.image,
      this.width,
      required this.time,
      required this.distance,
      required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double theWidth = width ?? kWidthFull(context);
    return ContainerWithShadow(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 141,
            decoration: BoxDecoration(
                borderRadius: kBorderRadius,
                image: DecorationImage(
                    fit: BoxFit.cover, image: NetworkImage(image))),
          ).paddingBottom(kSpacingSmall).paddingTop(10),
          GrigoraTextTitle(
            title,
            textSize: kFontSizeMedium,
            isLightFont: true,
          ).paddingBottom(kSpacingSmall),
          GrigoraTextTitle(
            subtitle,
            textSize: kFontSizeNormal,
            textColor: dimGrey,
            isLightFont: true,
          ).paddingBottom(kSpacingSmall),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(kImagesIconStar).paddingRight(5),
                    GrigoraText(rating)
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(kImagesIconTime).paddingRight(5),
                    GrigoraText(time)
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(kImagesIconMapDistance).paddingRight(5),
                    GrigoraText(distance)
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    )
        .onTap(onTap ?? () => print('tap to see single'))
        .withWidth(theWidth)
        .paddingRight(kSpacingMedium)
        .paddingBottom(kSpacingLarge);
  }
}


class TopOrdersCard extends StatelessWidget {
  final String title;
  final String image;
  final double? width;
  final Function()? onTap;
  final String rating;
  final String distance;
  const TopOrdersCard(
      {Key? key,
      required this.title,
      this.onTap,
      required this.image,
      this.width,
      required this.distance,
      required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double theWidth = width ?? kWidthFull(context);
    return ContainerWithShadow(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 141,
            decoration: BoxDecoration(
                borderRadius: kBorderRadius,
                image: DecorationImage(
                    fit: BoxFit.cover, image: NetworkImage(image))),
          ).paddingBottom(kSpacingSmall).paddingTop(10),
          GrigoraTextTitle(
            title,
            textSize: kFontSizeMedium,
            isLightFont: true,
          ).paddingBottom(kSpacingSmall),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(kImagesIconStar).paddingRight(5),
                    GrigoraText(rating)
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(kImagesIconMapDistance).paddingRight(5),
                    GrigoraText(distance)
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    )
        .onTap(onTap ?? () => print('tap to see single'))
        .withWidth(theWidth)
        .paddingRight(kSpacingMedium)
        .paddingBottom(kSpacingLarge);
  }
}
