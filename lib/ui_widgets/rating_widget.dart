import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';

class RatingWidget extends StatelessWidget {
  const RatingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(kImagesStarGoldIcon).paddingRight(kSpacingSmall),
        GrigoraText(
          '4.8',
          textSize: kFontSizeMedium,
        ).paddingRight(kSpacingSmall),
        GrigoraText(
          '140+ Ratings',
          textColor: Color(0xFF666666),
        )
      ],
    );
  }
}
