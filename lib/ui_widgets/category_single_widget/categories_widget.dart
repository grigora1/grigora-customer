import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';

class _ItemModel extends HorizontalScrollModel {
  final String image, title;
  _ItemModel({required this.image, required this.title});
}

class CategoriesWidget extends StatelessWidget {
  const CategoriesWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ItemModel> _items = [
      _ItemModel(
          title: 'Bakery & Cakes',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627290126/grigora/Image_3_trawxy.png'),
      _ItemModel(
          title: 'Fries & Grills',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627290123/grigora/Image_2_nofu17.png'),
      _ItemModel(
          title: 'Dishes',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627290102/grigora/Image_1_b8f9gx.png'),
      _ItemModel(
          title: 'Drinks',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627290101/grigora/Image_qvtoqr.png'),
    ];
    return GrigoraSectionWidget(
      title: 'Categories',
      hideTitle: true,
      child: GrigoraHorizontalScroll<_ItemModel>(
          items: _items,
          builder: (position, model) => _Widget(
                image: model.image,
                title: model.title,
                onTap: () => print('do nothing'),
              ).paddingRight(kSpacingMedium)),
    );
  }
}

class _Widget extends StatelessWidget {
  final String image, title;
  final Function()? onTap;
  const _Widget(
      {Key? key, required this.image, required this.title, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          children: [
            GrigoraNetworkImage(image).paddingBottom(kSpacingSmall),
            GrigoraText(
              title,
              isBoldFont: true,
            )
          ],
        )
      ],
    ).paddingRight(kSpacingSmall);
  }
}
