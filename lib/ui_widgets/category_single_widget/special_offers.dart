import 'package:flutter/material.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/views/cart/cart_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/constants.dart';

class SpecialOffers extends StatelessWidget {
  final double? width;
  const SpecialOffers({Key? key, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<RestaurantWidgetModel> _items = [
      RestaurantWidgetModel(
          title: 'Chicken & Chips',
          time: '32 Mins',
          rating: '4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Chicken & Chips',
          time: '32 Mins',
          rating: ' 4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1572695064956-52bfdd19cfee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Chicken & Chips',
          time: '32 Mins',
          rating: '4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1573555957315-723d970bcdde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Chicken & Chips',
          time: '32 Mins',
          rating: '4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1422919869950-5fdedb27cde8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    ];

    return GrigoraSectionWidget(
        title: 'Special Offers',
        showAllAction: () => print('goto show all'),
        child: GrigoraHorizontalScroll<RestaurantWidgetModel>(
            builder: (position, model) => PopularRestaurantItem(
                width: width ?? kWidthFull(context) * 0.6,
                title: _items[position].title,
                time: _items[position].time,
                rating: _items[position].rating,
                distance: _items[position].distance,
                image: _items[position].image,
                subtitle: _items[position].category,
                onTap: () => UiUtils.goToWidget(CartScreen(
                      item: VendorWidgetModel(
                          title: 'Burgers',
                          time: '32 Mins',
                          address: 'No5, st Gogery Rd. beside G',
                          rating: 4.8,
                          category: 'Fast Food',
                          image:
                              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
                    ))),
            items: _items));
  }
}
