import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/home/filter_screen.dart';
import 'package:nb_utils/nb_utils.dart';

class FilterWidget extends StatelessWidget {
  const FilterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: _Dropdown(
            title: 'Vendors',
          ).paddingRight(kPaddingSmall),
        ),
        Expanded(
          child: _Dropdown(
            title: 'Over 4.5',
          ).paddingRight(kPaddingSmall),
        ),
        _Button(),
      ],
    );
  }
}

// class _Dropdown extends StatelessWidget {
//   const _Dropdown({ Key? key }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(

//     );
//   }
// }

class _Dropdown extends StatelessWidget {
  final String title;
  const _Dropdown({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllMedium,
      decoration: BoxDecoration(
          borderRadius: kBorderRadius, color: kColorBlack.withOpacity(0.05)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GrigoraText(
            title,
            isBoldFont: true,
          ),
          Icon(EvaIcons.chevronDown)
        ],
      ),
    );
  }
}

class _Button extends StatelessWidget {
  const _Button({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => UiUtils.goToWidget(
        FilterScreen(),
      ),
      child: Container(
        padding: kPaddingAllMedium,
        decoration: BoxDecoration(
            borderRadius: kBorderRadius, color: kColorBlack.withOpacity(0.05)),
        child: Icon(EvaIcons.optionsOutline),
      ),
    );
  }
}
