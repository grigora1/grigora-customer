import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';

class RestaurantNearby extends StatelessWidget {
  const RestaurantNearby({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<VendorWidgetModel> _items = [
      VendorWidgetModel(
          title: 'Burger King Hangout spot',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Mcdonalds Hangout spot',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1573555957315-723d970bcdde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Leventis Restuarant',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1572695064956-52bfdd19cfee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Radison Blu Hotels',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1422919869950-5fdedb27cde8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    ];

    return GrigoraSectionWidget(
        title: 'Restaurant Nearby',
        showAllAction: () => print('go somewhere'),
        child: ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, position) {
                  return VendorItem(
                    title: _items[position].title,
                    time: _items[position].time,
                    address: _items[position].address!,
                    rating: _items[position].rating!,
                    category: _items[position].category!,
                    image: _items[position].image,
                    onTap: () => print('hello'),
                  );
                },
                separatorBuilder: (context, position) {
                  return SizedBox(
                    height: kSpacingMedium,
                  );
                },
                itemCount: _items.length)
            .paddingSymmetric(horizontal: kSpacingMedium));
  }
}
