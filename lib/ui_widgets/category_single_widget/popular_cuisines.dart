import 'package:flutter/material.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/constants.dart';

class PopularCuisines extends StatelessWidget {
  const PopularCuisines({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<RestaurantWidgetModel> _items = [
      RestaurantWidgetModel(
          title: 'Burger King Hangout spot',
          time: '32 Mins',
          rating: '4.8',

          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Leventis Restuarant',
          time: '32 Mins',
          rating: ' 4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1572695064956-52bfdd19cfee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Mcdonalds Hangout spot',
          time: '32 Mins',
          rating: '4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1573555957315-723d970bcdde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      RestaurantWidgetModel(
          title: 'Radison Blu Hotels',
          time: '32 Mins',
          rating: '4.8',
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1422919869950-5fdedb27cde8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    ];

    return GrigoraSectionWidget(
        title: 'Popular Cuisines',
        showAllAction: () => print('goto show all'),
        child: GrigoraHorizontalScroll<RestaurantWidgetModel>(
            builder: (position, model) => PopularRestaurantItem(
                  width: kWidthFull(context) * 0.6,
                  title: _items[position].title,
                  time: _items[position].time,
                  image: _items[position].image,
                  subtitle: _items[position].category,
                  distance: _items[position].distance,
                  rating: _items[position].rating,
                  onTap: () => UiUtils.goToWidget(
                      RestaurantSingleScreen(item: _items[position])),
                ),
            items: _items));
  }
}
