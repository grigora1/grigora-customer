import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/container_with_shadow.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class OfferWidgetModel {
  final String title, time;

  final String? image, category, minOrder, offer;
  final double? rating;
  const OfferWidgetModel(
      {Key? key,
      required this.title,
      this.rating,
      this.offer,
      this.minOrder,
      this.category,
      this.image,
      required this.time});
}

class OfferItem extends StatelessWidget {
  final String title, time, offer, minOrder;
  final String? image;
  final double rating;
  final Function() onTap;
  const OfferItem(
      {Key? key,
      required this.title,
      required this.onTap,
      this.rating = 4.8,
      this.minOrder = 'Min Order: 5000',
      this.offer = '20% off all products',
      this.image,
      required this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: ContainerWithShadow(
        child: Row(
          children: [
            Container(
              height: 120,
              width: 150,
              decoration: BoxDecoration(
                  borderRadius: kBorderRadius,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(image ?? kImagesRemotePlaceholder))),
            ),
            UIHelper.horizontalSpaceMedium(),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // GrigoraTextTitle(
                  //   title,
                  //   maxLines: 1,
                  //   overflow: TextOverflow.ellipsis,
                  //   textSize: kFontSizeMedium,
                  // ),
                  // UIHelper.verticalSpaceSmall(),
                  GrigoraText(offer, textColor: kColorGreen,),
                  UIHelper.verticalSpaceMedium(),
                  GrigoraText(
                    title,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  UIHelper.verticalSpaceSmall(),
                  GrigoraText(
                    minOrder,
                    maxLines: 1,
                    textColor: kColorPrimaryRed,
                    overflow: TextOverflow.ellipsis,
                  ),
                  UIHelper.verticalSpaceSmall(),
                  Row(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.star,
                            color: kColorGold,
                          ),
                          GrigoraText(rating.toString())
                        ],
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      Row(
                        children: [
                          Icon(Icons.car_rental, color: kColorPurple),
                          GrigoraText(time)
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
