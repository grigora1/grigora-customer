import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/views/about_screen.dart';
import 'package:grigora/views/country_select/country_select_screen.dart';
import 'package:grigora/views/create_account/create_account_screen.dart';
import 'package:grigora/views/help_screen.dart';
import 'package:grigora/views/home/filter_screen.dart';
import 'package:grigora/views/home/home_screen.dart';
import 'package:grigora/views/login_email/login_email_screen.dart';
import 'package:grigora/views/notifications.dart';
import 'package:grigora/views/login/login_screen.dart';
import 'package:grigora/views/forgot_password/forgot_password_screen.dart';
import 'package:grigora/views/orders/orders_screen.dart';
import 'package:grigora/views/pay/pay_screen.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_screen.dart';
import 'package:grigora/views/restaurants/restaurants_screen.dart';
import 'package:grigora/views/settings/settings_screen.dart';
import 'package:grigora/views/onboarding_screen.dart';
import 'package:grigora/views/order_summary/order_summary_screen.dart';
import 'package:grigora/views/verify_otp/verify_otp_screen.dart';
import 'package:grigora/views/checkout/checkout_screen.dart';
import 'package:grigora/views/delivery_address/delivery_address_screen.dart';
import 'package:grigora/views/track_order/track_order_screen.dart';
import 'package:grigora/views/track_order_summary/track_order_summary_screen.dart';
import 'package:grigora/views/card_details/card_details_screen.dart';

class AppPages {
  AppPages._();
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case kRouteLogin:
        return new MyCustomRoute(
          builder: (_) => new LoginScreen(),
          settings: settings,
        );
      case kRouteLoginEmail:
        return new MyCustomRoute(
          builder: (_) => new LoginEmailScreen(),
          settings: settings,
        );
      case kRouteCreateAccount:
        return new MyCustomRoute(
          builder: (_) => new CreateAccountScreen(),
          settings: settings,
        );
      case kRouteCountrySelect:
        return new MyCustomRoute(
          builder: (_) => new CountrySelectScreen(),
          settings: settings,
        );
      case kRouteForgotPassword:
        return new MyCustomRoute(
          builder: (_) => new ForgotPasswordScreen(),
          settings: settings,
        );
      case kRouteHome:
        return new MyCustomRoute(
          builder: (_) => new HomeScreen(),
          settings: settings,
        );
      case kRouteRestaurants:
        return new MyCustomRoute(
          builder: (_) => new RestaurantsScreen(),
          settings: settings,
        );
      // case kRouteRestaurantSingle:
      //   return new MyCustomRoute(
      //     builder: (_) => new RestaurantSingleScreen(),
      //     settings: settings,
      //   );
      case kRouteAbout:
        return new MyCustomRoute(
          builder: (_) => new AboutScreen(),
          settings: settings,
        );
      case kRouteHelp:
        return new MyCustomRoute(
          builder: (_) => new HelpScreen(),
          settings: settings,
        );
      case kRouteSettings:
        return new MyCustomRoute(
          builder: (_) => new SettingsScreen(),
          settings: settings,
        );
      case kRouteFilter:
        return new MyCustomRoute(
          builder: (_) => new FilterScreen(),
          settings: settings,
        );
      case kRoutePay:
        return new MyCustomRoute(
          builder: (_) => new PayScreen(),
          settings: settings,
        );
      case kRouteOrders:
        return new MyCustomRoute(
          builder: (_) => new OrdersScreen(),
          settings: settings,
        );
      case kRouteNotifications:
        return new MyCustomRoute(
          builder: (_) => new NotificationsScreen(),
          settings: settings,
        );
      case kRouteOrderSummary:
        return new MyCustomRoute(
          builder: (_) => new OrderSummaryScreen(),
          settings: settings,
        );
      case kRouteCheckout:
        return new MyCustomRoute(
          builder: (_) => new CheckoutScreen(),
          settings: settings,
        );
      case kRouteDeliveryAddress:
        return new MyCustomRoute(
          builder: (_) => new DeliveryAddressScreen(),
          settings: settings,
        );
      case kRouteTrackOrder:
        return new MyCustomRoute(
          builder: (_) => new TrackOrderScreen(),
          settings: settings,
        );
      case kRouteTrackOrderSummary:
        return new MyCustomRoute(
          builder: (_) => new TrackOrderSummaryScreen(),
          settings: settings,
        );
      case kRouteCardDetails:
        return new MyCustomRoute(
          builder: (_) => new CardDetailsScreen(),
          settings: settings,
        );
      // case kRouteVerifyOTP:
      //   return new MyCustomRoute(
      //     builder: (_) => new VerifyOTP(),
      //     settings: settings,
      //   );
      default:
        return new MyCustomRoute(
          builder: (_) => new OnboardingScreen(),
          settings: settings,
        );
    }
    // switch (settings.name) {
    //   case '/':
    //     return GetRoute(
    //       builder: (_) => SplashScreen(),
    //       settings: settings,
    //     );
    //   case '/Home':
    //     return GetRoute(settings: settings, builder: (_) => Home(), transition: Transition.fade);
    //   case '/Chat':
    //     return GetRoute(settings: settings, builder: (_) => Chat(),transition: Transition.rightToLeft);
    //   default:
    //     return GetRoute(
    //         settings: settings,
    //         transition: Transition.rotate
    //         builder: (_) => Scaffold(
    //               body: Center(
    //                   child: Text('No route defined for ${settings.name}')),
    //             ));
    // }
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({required WidgetBuilder builder, RouteSettings? settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
