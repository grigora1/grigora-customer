class ArgsVerifyOTP {
  final String email, token, phone;
  ArgsVerifyOTP(
      {required this.email, required this.token, required this.phone});
}

class ArgsCategory {
  final String title;
  ArgsCategory({required this.title});
}
