import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

const String kThemeLocal = 'THEME_DATA';
const String kAppName = 'Grigora Vendor';


//App constants
const double kMaximumFilterDistance = 50.0;

// @todelete: Spacing
const double kPadding = 5;
const double kSmallPadding = 10;
const double kRegularPadding = 15;
const double kMediumPadding = 20;
const double kBoldPadding = 35;
const double kLargePadding = 40;
const double kWidthRatio = 0.9;
const double kIconSize = 24;

const double kPaddingSmall = 10;
const double kPaddingMedium = 20;
const double kPaddingLarge = 30;
const double kPaddingExtraLarge = 150;

const double kSpacingSmall = 10;
const double kSpacingMedium = 20;
const double kSpacingLarge = 40;
const double kSpacingExtraLarge = 150;

// Border
const double kBorderWidth = 1;
const double kThickBorderWidth = 3;
const BorderRadius kBorderRadius =
    BorderRadius.all(const Radius.circular(kSmallPadding));
const BorderRadius kFullBorderRadius =
    BorderRadius.all(const Radius.circular(100));
final BoxDecoration kTextFieldBoxDecoration = BoxDecoration(
    borderRadius: kBorderRadius,
    border: Border.all(color: kThemeData.dividerColor),
    color: Colors.white);
final BoxDecoration kButtonBoxDecoration = BoxDecoration(
    borderRadius: kFullBorderRadius, border: Border.all(color: kColorPrimary));
final BoxDecoration kBottomSheetBoxDecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: new BorderRadius.only(
    topLeft: const Radius.circular(25.0),
    topRight: const Radius.circular(25.0),
  ),
);

// Box Shadow
BoxShadow kBoxShadow(Color color) => BoxShadow(
      color: color,
      spreadRadius: 0,
      blurRadius: 15,
      offset: Offset(0, 2), // changes position of shadow
    );

// Colors
const Color kColorPrimary = Color(0xFFCA3114);
const Color kColorPrimaryLight = Color(0xFFF7D6D6);
const Color kColorPurple = Color(0xFFA5A6F6);
const Color kColorGold = Color(0xFFFD9942);
const Color kColorPrimaryText = Colors.black;
final Color kColorWhite = Colors.white;
const Color kSecondaryTextColor = Colors.white;
const Color kScaffoldBackgroundColor = Colors.white;
final Color kSubtextColor = Colors.grey[400]!;
final Color kColorBlack = Colors.black;
final Color kColorGrey = Color(0x24999999);
final Color kColorGreyLight = Colors.grey[100]!;
final Color kColorGreen = Color(0xFF3CC13B);
final Color kColorDarkGrey = Color(0xFF707070);
final Color kColorFacebook = Color(0xFF4267B2);
final Color kColorGoogle = Color(0xFF4285F4);
final Color kColorPrimaryRed = Color(0xFFD73133);
final Color kColorBackgroundGrey = Color(0xFFF2F2F2);
final Color kColorGenericGrey = Color(0xFF666666);

// FontSize
const double kFontSizeSmall = 12;
const double kFontSizeNormal = 14;
const double kFontSizeMedium = 16;
const double kFontSizeLarge = 20;
final double kFontSizeBig = 24;

// fonts
const String kFontFamilyLight = 'AvertaStd-Light';
const String kFontFamilyRegular = 'AvertaStd-Regular';
const String kFontFamilySemibold = 'AvertaStd-Semibold';

// Text
final TextStyle kHeadline1TextStyle = TextStyle(
    fontSize: 30, fontWeight: FontWeight.bold, color: kColorPrimaryText);
final TextStyle kHeadline2TextStyle = TextStyle(
    fontSize: 25, fontWeight: FontWeight.normal, color: kColorPrimaryText);
final TextStyle kHeadline3TextStyle = TextStyle(
    fontSize: 21, fontWeight: FontWeight.bold, color: kColorPrimaryText);
final TextStyle kBodyText1Style =
    TextStyle(fontSize: 16, color: kColorPrimaryText);
final TextStyle kBodyText2Style =
    TextStyle(fontSize: 14, color: kColorPrimaryText);
final TextStyle kBodyText3Style = TextStyle(
    fontSize: 18, fontWeight: FontWeight.bold, color: kColorPrimaryText);
final TextStyle kSubtitle1Style = TextStyle(fontSize: 12, color: kSubtextColor);
final TextStyle kSubtitle2Style =
    TextStyle(fontSize: 12, color: kColorPrimaryText);

// Misc functions
String kPriceFormatter(double price) =>
    '\$' + NumberFormat("#,##0.00", "en_US").format(price);
List kBuilderName(String name) => name.split(' ');
double kCalculatedWidth(Size size) => size.width * kWidthRatio;
double kCalculatedMargin(Size size) => size.width * (1 - kWidthRatio) / 2;

// Theme
final ThemeData kThemeData = ThemeData.light().copyWith(
    primaryColor: kColorPrimary,
    scaffoldBackgroundColor: kScaffoldBackgroundColor,
    dividerColor: Colors.grey[350]!,
    
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kIconSize),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle,
        headline2: kHeadline2TextStyle,
        headline3: kHeadline3TextStyle,
        bodyText1: kBodyText1Style,
        bodyText2: kBodyText2Style,
        subtitle1: kSubtitle1Style,
        subtitle2: kSubtitle2Style));

final ThemeData kThemeDataDark = ThemeData.dark().copyWith(
    primaryColor: kColorPrimary,
    primaryColorDark: Colors.black,
    primaryColorLight: Colors.white,
    scaffoldBackgroundColor: Colors.grey[900],
    dividerColor: Colors.grey[350]!,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    iconTheme: IconThemeData(size: kIconSize),
    textTheme: TextTheme(
        headline1: kHeadline1TextStyle.copyWith(color: Colors.white),
        headline2: kHeadline2TextStyle.copyWith(color: Colors.white),
        headline3: kHeadline3TextStyle.copyWith(color: Colors.white),
        bodyText1: kBodyText1Style.copyWith(color: Colors.white),
        bodyText2: kBodyText2Style.copyWith(color: Colors.white),
        subtitle1: kSubtitle1Style.copyWith(color: Colors.white),
        subtitle2: kSubtitle2Style.copyWith(color: Colors.white)));

// Routes
const kRouteOnboarding = "/onboarding";
const kRouteCountrySelect = "/country-select";
const kRouteLogin = '/login';
const kRouteLoginEmail = '/login-email';
const kRouteCreateAccount = '/create-account';
const kRouteForgotPassword = "/forgot-password";
const kRouteHome = '/home';
const kRouteRestaurants = '/restaurants';
const kRouteAbout = '/about';
const kRouteHelp = '/help';
const kRouteSettings = '/settings';
const kRoutePay = '/pay';
const kRouteOrders = '/orders';
const kRouteNotifications = '/notifications';
const kRouteVerifyOTP = '/verify-otp';
const kRouteRestaurantSingle = '/restaurant-single';
const kRouteFilter = '/filter';
const kRouteOrderSummary = '/order-summary';
const kRouteCheckout = '/checkout';
const kRouteDeliveryAddress = '/delivery-address';
const kRouteTrackOrder = '/track-order';
const kRouteTrackOrderSummary = '/track-order-summary';
const kRouteCardDetails = '/card-details';

// Images
const String kImagesGrigoraLogo = 'images/logo.png';
const String kImagesGoogleIcon = 'images/icon-google.png';
const String kImagesFacebookIcon = 'images/icon-facebook.svg';
const String kImagesAppleIcon = 'images/icon-apple.svg';
const String kImagesPay = 'images/pay.svg';
const String kImagesLogin = 'images/login.svg';
const String kImagesPlaceholder = 'images/placeholder.png';
const String kImagesRemotePlaceholder =
    'https://media.istockphoto.com/vectors/thumbnail-image-vector-graphic-vector-id1147544807?k=6&m=1147544807&s=612x612&w=0&h=8CXEtGfDlt7oFx7UyEZClHojvDjZR91U-mAU8UlFF4Y=';
const String kImagesRice = 'images/rice-section-1.jpg';
const String kImagesBreakfast = 'images/breakfast-section-1.jpg';
const String kImagesCard = 'images/card.png';

// svg_icons
const String kImagesIconLocation = 'images/icon-location.svg';
const String kImagesIconCart = 'images/icon-cart.svg';
const String kImagesIconChat = 'images/icon-chat.svg';
const String kImagesIconVerified = 'images/icon-verified.svg';
const String kImagesIconCashPayment = 'images/icon-cash-payment.svg';
const String kImagesIconCardPayment = 'images/icon-card-payment.svg';
const String kImagesIconClock = 'images/icon-clock.svg';
const String kImagesIconSchedule = 'images/icon-schedule.svg';
const String kImagesIconNaira = 'images/icon-naira.svg';
const String kImagesIconStart = 'images/icon-start.svg';
const String kImagesIconEnd = 'images/icon-end.svg';
const String kImagesIconMapDistance = 'images/icon-map-distance.svg';
const String kImagesIconStar = 'images/icon-star.svg';
const String kImagesIconTime = 'images/icon-time.svg';
const String kImagesBarcodeScan = 'images/barcode-icon.svg';
const String kImagesFilterIcon = 'images/filter-icon.svg';
const String kImagesStarGoldIcon = 'images/star-gold.svg';
const String kImagesStarWhiteIcon = 'images/star-white.svg';
const String kImagesPrice1 = 'images/price-1.svg';
const String kImagesPrice2 = 'images/price-2.svg';
const String kImagesPrice3 = 'images/price-3.svg';
const String kImagesDot = 'images/dot.svg';
const String kImagesExpensiveIcon = 'images/expensive-icon.svg';
const String kImagesTimingIcon = 'images/timing-icon.svg';
const String kImagesBikeIcon = 'images/bike-icon.svg';
const String kImagesSearchIcon = 'images/search-icon.svg';
const String kImagesOrdersIcon = 'images/orders-icon.svg';
const String kImagesOffersIcon = 'images/offers-icon.svg';
const String kImagesMarketIcon = 'images/market-icon.svg';
const String kImagesAccountIcon = 'images/account-icon.svg';
const String kImagesCardIcon = 'images/card-icon.svg';


// width
double kWidthFull(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

// misc functions
kGotoScreen(BuildContext context, String routeName, {Object? arguments}) {
  Navigator.pushNamed(context, routeName, arguments: arguments);
}

// padding
const EdgeInsetsGeometry kPaddingAllSmall = const EdgeInsets.all(kSmallPadding);
const EdgeInsetsGeometry kPaddingAllMedium =
    const EdgeInsets.all(kMediumPadding);
const EdgeInsetsGeometry kPaddingAllLarge = const EdgeInsets.all(kLargePadding);

// misc constants
const TextAlign kTextAlignCenter = TextAlign.center;

// Endpoint
const _base_url = 'http://143.110.237.114:4001/api/v1';
const kEndpointRegister = _base_url + '/users/auth/register';
const kEndpointLogin = _base_url + '/users/auth/login';
const kEndpointVerifyOTP = _base_url + '/users/auth/validate_otp';
const kEndpointResendOTP = _base_url + '/users/auth/resend';
