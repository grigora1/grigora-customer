import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/utils/ui_helper.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
        elevation: 0,
      ),
      body: Padding(
        padding: kPaddingAllMedium,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle('Forgot password'),
            UIHelper.verticalSpaceLarge(),
            _Form()
          ],
        ),
      ),
    );
  }
}

class _Form extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Form(
      // key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(placeholder: 'Email'),
          Divider(
            color: kColorDarkGrey,
          ),
          UIHelper.verticalSpaceMedium(),
          GrigoraButton(
            onTap: () => print('do nothing'),
            text: 'Reset your password',
            borderRadius: kBorderRadius,
          ),
        ],
      ),
    );
  }
}
