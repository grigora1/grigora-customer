import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/track_order_widget.dart';
import 'package:grigora/ui_widgets/user_info_widget.dart';

class TrackOrderSingleScreen extends StatelessWidget {
  const TrackOrderSingleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Chicken Republic',
      withStack: true,
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TabOrderSingleWidget(),
              UserInfoWidget().paddingTop(kSpacingMedium),
              _OrderInProgress().paddingTop(kSpacingLarge)
            ],
          ).paddingAll(kSpacingMedium),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: GrigoraButtonExtended(
                    title: 'Track Order Progress',
                    onTap: () => UiUtils.goto(kRouteTrackOrderSummary))
                .paddingAll(kSpacingMedium),
          )
        ],
      ),
    );
  }
}

class _OrderInProgress extends StatelessWidget {
  const _OrderInProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Order in Progress',
          textSize: kFontSizeMedium,
        ),
        GrigoraText('Twenty minutes ago (10:00am)').paddingTop(5),
        _Timeline().paddingTop(kSpacingMedium)
      ],
    );
  }
}

class _Timeline extends StatelessWidget {
  const _Timeline({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _TimelineItem(
          title: 'Restaurant has accepted your order',
          time: '10:00am',
        ),
        _TimelineItem(
          title: 'Micheal has served your food',
          time: '10:05am',
        ),
        _TimelineItem(
          title: 'Restaurant has processed your order',
          isActive: true,
          time: '10:15am',
        ),
        _TimelineItem(
          title: 'Micheal has served your food',
          isLastItem: true,
          time: '10:20am',
        ),
      ],
    );
  }
}

class _TimelineItem extends StatelessWidget {
  final bool isLastItem, isActive;
  final String title;
  final String time;
  const _TimelineItem(
      {Key? key,
      required this.title,
      required this.time,
      this.isActive = false,
      this.isLastItem = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              time,
              textSize: kFontSizeSmall,
              textAlign: TextAlign.right,
            ).withWidth(70).paddingRight(kSpacingMedium),
            Container(
              decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: kColorPrimary))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GrigoraText(
                    title,
                    isBoldFont: true,
                    textSize: isActive ? kFontSizeMedium + 2 : kFontSizeMedium,
                    textColor: isActive ? kColorPrimary : kColorBlack,
                  ).paddingSymmetric(horizontal: kSpacingMedium),
                  Container(
                    height: 50,
                  ).visible(!isLastItem),
                ],
              ).expand(),
            ).expand(),
          ],
        ).paddingSymmetric(horizontal: kSpacingSmall),
        Positioned(
            top: 0,
            left: 92,
            child: _CheckWidget(
              isActive: isActive,
            )),
      ],
    );
  }
}

class _CheckWidget extends StatelessWidget {
  final bool isActive;
  const _CheckWidget({Key? key, this.isActive = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(const Radius.circular(5)),
          color: isActive ? kColorPrimary : kColorPrimaryLight),
      child: Icon(
        EvaIcons.checkmark,
        color: isActive ? kColorWhite : kColorPrimary,
        size: isActive ? 20 : 10,
      ),
    );
  }
}
