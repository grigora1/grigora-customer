import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/login/login_vm.dart';
import 'package:nb_utils/nb_utils.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginVM>(
      model: LoginVM(),
      builder: (context, model, child) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
            appBar: new AppBar(
              iconTheme: IconThemeData(color: kColorBlack),
              centerTitle: true,
              backgroundColor: kColorWhite,
              elevation: 0,
              actions: [_CheckOutAsGuest()],
            ),
            body: SafeArea(child: _LoginWidget()),
          ),
        );
      },
    );
  }
}

class _CheckOutAsGuest extends StatelessWidget {
  const _CheckOutAsGuest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => UiUtils.goto(kRouteCheckout),
      child: GrigoraText(
        'Checkout as guest',
        textColor: kColorPrimary,
        isBoldFont: true,
      ).paddingAll(kPaddingMedium),
    );
  }
}

class _LoginWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(kRegularPadding),
      child: Column(
        children: [
          Expanded(
            child: Image.asset(
              kImagesGrigoraLogo,
              width: 250,
            ),
          ),
          UIHelper.verticalSpaceExtraLarge(),
          GrigoraTextTitle('Log in or create an account')
              .paddingBottom(kPaddingSmall),
          GrigoraText(
            'Login or create an account to recieve rewards and save your details for a faster checkout experience',
            textAlign: kTextAlignCenter,
          ).paddingBottom(kPaddingLarge),
          GrigoraButton(
            onTap: () => kGotoScreen(context, kRouteCreateAccount),
            text: 'Continue an account',
            withBg: true,
          ).paddingBottom(kPaddingLarge),
          GrigoraButton(
            onTap: () => kGotoScreen(context, kRouteLoginEmail),
            text: 'Login',
          ).paddingBottom(kPaddingExtraLarge),
        ],
      ),
    );
  }
}

class SkipButtonText extends StatelessWidget {
  final bool isInactive;

  SkipButtonText({this.isInactive = false});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, kRouteRestaurants),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text(
          'SKIP',
          style: TextStyle(
              color: isInactive ? kColorWhite : kColorBlack,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

class _AppTitleText extends StatelessWidget {
  final bool isActive;
  final String text;
  final Function() onTap;
  _AppTitleText(
      {this.isActive = false, required this.onTap, required this.text});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        decoration: BoxDecoration(
            color: isActive ? kColorBlack : kColorGrey,
            borderRadius: BorderRadius.circular(20)),
        child: Text(
          text,
          style: TextStyle(
              color: isActive ? kColorWhite : kColorBlack,
              fontWeight: FontWeight.bold,
              fontSize: kFontSizeMedium),
        ),
      ),
    );
  }
}
