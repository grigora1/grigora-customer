import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/add_address/add_address_screen.dart';
import 'package:grigora/views/edit_profile/edit_profile_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';

class SavedAddressScreen extends StatelessWidget {
  const SavedAddressScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return LayoutWithBack(
      title: 'Address',
      withStack: false,
      withShoppingCart: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              'Saved Addresses',
              textAlign: TextAlign.start,
              textSize: 18,
            ).paddingLeft(kSpacingMedium).paddingTop(kSpacingMedium),
            _AddressCell(
              onTap: () {
                UiUtils.goToWidget(
                  AddAddressScreen(),
                );
              },
              addressNumber: 1,
              addressTitle: 'Quegura Lounge, Lagos',
              address1: '26 Boudillion Roads Kings Building,',
              address2: 'Floor 7, Terry terrains',
              mobileNumber: '+972 3978291736',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _AddressCell(
              onTap: () {
                UiUtils.goToWidget(
                  AddAddressScreen(),
                );
              },
              addressNumber: 2,
              addressTitle: 'Quegura Lounge, Lagos',
              address1: '26 Boudillion Roads Kings Building,',
              address2: 'Floor 7, Terry terrains',
              mobileNumber: '+972 3978291736',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    UiUtils.goToWidget(
                      AddAddressScreen(),
                    );
                  },
                  child: GrigoraTextTitle(
                    'Add New Address',
                    textAlign: TextAlign.center,
                    textSize: 14,
                    textColor: kColorPrimaryRed,
                  ).paddingTop(kSpacingLarge),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _AddressCell extends StatelessWidget {
  final String addressTitle;
  final String address1;
  final String address2;
  final String mobileNumber;
  final int addressNumber;
  final Function() onTap;
  const _AddressCell(
      {Key? key,
      this.addressTitle = '',
      this.address1 = '',
      this.address2 = '',
      required this.addressNumber,
      this.mobileNumber = '',
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              'ADDRESS $addressNumber',
              textColor: kColorGenericGrey,
              textAlign: TextAlign.start,
              textSize: 11,
            ).paddingBottom(kSpacingMedium),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle(
                      addressTitle,
                      textAlign: TextAlign.start,
                      textSize: 16,
                    ),
                    Visibility(
                      visible: this.address1 != '',
                      child: GrigoraText(
                        address1,
                        textColor: kColorGenericGrey,
                        textAlign: TextAlign.start,
                        textSize: 14,
                      ).paddingTop(kSpacingSmall),
                    ),
                    GrigoraText(
                      address2,
                      textColor: kColorGenericGrey,
                      textAlign: TextAlign.start,
                      textSize: 14,
                    ).paddingTop(kSpacingSmall),
                    GrigoraText(
                      'Mobile Number: $mobileNumber',
                      textColor: kColorGenericGrey,
                      textAlign: TextAlign.start,
                      textSize: 14,
                    ).paddingTop(kSpacingSmall),
                  ],
                ),
                Icon(
                  Icons.chevron_right,
                  size: 18,
                )
              ],
            ),
            Container(
              height: 0.2,
              decoration: BoxDecoration(color: kColorGenericGrey),
            ).paddingTop(kSpacingMedium)
          ],
        ).paddingTop(kSpacingSmall),
      ),
    );
  }
}
