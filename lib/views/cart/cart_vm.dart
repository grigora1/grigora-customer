import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_model.dart';

class CartVM extends BaseModel {
  final VendorWidgetModel item;
  CartVM(this.item);

  double _price = 2000;
  double get price => _price;
  set price(double value) {
    _price = value;
    notifyListeners();
  }

  int _quantity = 1;
  int get quantity => _quantity;
  set quantity(int value) {
    _quantity = value;
    notifyListeners();
  }

  // @todo: add check list
  double get totalAMount => price * quantity;
}
