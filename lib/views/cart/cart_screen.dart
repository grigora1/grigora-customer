import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/views/cart/cart_vm.dart';
import 'package:provider/provider.dart';
import 'package:grigora/ui_widgets/rating_widget.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/quantity_selector_widget.dart';
import 'package:grigora/ui_widgets/grigora_cart_choice_table.dart';
import 'package:grigora/ui_widgets/additional_message.dart';
import 'package:grigora/ui_widgets/you_might_also_like.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_nested_scroll.dart';

class CartScreen extends StatelessWidget {
  final VendorWidgetModel item;
  const CartScreen({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartVM>(
        model: CartVM(item),
        builder: (context, model, child) {
          return LayoutWithNestedScroll(
              backgroundWidget: Container(
                decoration: BoxDecoration(
                    image: UiUtils.grigoraNetworkImageProvider(item.image)),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
              ),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _Title(),
                  _Categories().paddingTop(kSpacingSmall),
                  _Description().paddingTop(kSpacingSmall),
                  RatingWidget().paddingTop(kSpacingSmall),
                  _DiscountInfo().paddingTop(kSpacingSmall),
                  _Price().paddingTop(kSpacingSmall),
                  GrigoraTextTitle('Choice of Add On')
                      .paddingTop(kSpacingLarge),
                  _ChoiceWidget(
                    title: 'Meat',
                  ).paddingTop(kSpacingMedium),
                  AdditionalMessage().paddingTop(kSpacingMedium),
                  GrigoraButtonExtended(
                      onTap: () =>
                          UiUtils.showBottomSheet(body: _UpsellItems()),
                      title: 'Add to cart',
                      rightWidget: GrigoraText(
                        '${model.totalAMount}NGN',
                        textColor: kColorWhite,
                      )).paddingSymmetric(vertical: kSpacingLarge),
                ],
              ).paddingAll(kSpacingMedium));
        });
  }
}

class _Title extends StatelessWidget {
  const _Title({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<CartVM>(context);
    return GrigoraTextTitle(model.item.title);
  }
}

class _Categories extends StatelessWidget {
  const _Categories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _List(
          text: 'BURGERS',
          hideIcon: true,
        ),
        _List(text: 'FAST FOOD'),
      ],
    );
  }
}

class _List extends StatelessWidget {
  final String text;
  final bool hideIcon;
  const _List({Key? key, required this.text, this.hideIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.circle,
          size: 10,
        ).paddingRight(5).visible(!hideIcon),
        GrigoraText(text),
      ],
    ).paddingRight(kSpacingSmall);
  }
}

class _Description extends StatelessWidget {
  const _Description({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Description',
          textSize: kFontSizeMedium,
        ).paddingBottom(kSpacingSmall),
        GrigoraText(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tellus egestas ultrices tristique ornare congue adipiscing tincidunt et.')
      ],
    );
  }
}

class _DiscountInfo extends StatelessWidget {
  const _DiscountInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Icon(
              EvaIcons.pricetagsOutline,
            ).paddingRight(kSpacingSmall),
            GrigoraTextTitle(
              'Discount: 50% on purchase about N5000',
              textColor: kColorPrimary,
              textSize: kFontSizeNormal,
            )
          ],
        ).paddingBottom(kSpacingSmall),
        Container(
          color: kColorBlack.withOpacity(0.1),
          width: kWidthFull(context),
          height: 1,
        )
      ],
    );
  }
}

class _Price extends StatelessWidget {
  const _Price({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<CartVM>(context);
    return Container(
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          color: kColorGrey.withOpacity(0.4), borderRadius: kBorderRadius),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          QuantitySelectorWidget(
            callback: (val) => model.quantity = val,
          ),
          GrigoraTextTitle('NGN${model.quantity * model.price}')
        ],
      ),
    );
  }
}

class _ChoiceWidget extends StatelessWidget {
  final String title;
  const _ChoiceWidget({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(title),
        GrigoraText(
          'You are required to pick at least one option',
          textColor: kColorPrimary,
        ).paddingTop(kSpacingSmall),
        GrigoraCartChoiceTable(
          thead: ['Type', 'Price', 'Quantity'],
          tbody: [
            GrigoraCartChoiceTableModel(title: 'Chicken', price: 300),
            GrigoraCartChoiceTableModel(title: 'Beef', price: 300),
            GrigoraCartChoiceTableModel(title: 'Source', price: 300),
            GrigoraCartChoiceTableModel(title: 'Beef', price: 300),
          ],
        ).paddingTop(kSpacingMedium),
        GrigoraCartChoiceTable(
          thead: ['Drinks', 'Price', 'Quantity'],
          tbody: [
            GrigoraCartChoiceTableModel(title: 'Coke', price: 300),
            GrigoraCartChoiceTableModel(title: 'Sprite', price: 300),
            GrigoraCartChoiceTableModel(title: 'Youghurt', price: 300),
          ],
        ).paddingTop(kSpacingMedium),
      ],
    );
  }
}

class _UpsellItems extends StatelessWidget {
  const _UpsellItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllMedium,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle('You might also want to add...')
                        .paddingBottom(kSpacingSmall),
                    GrigoraText(
                      'would you want to add these as extra to your orders?',
                      textSize: kFontSizeMedium,
                    )
                  ],
                ),
              ),
              Container(
                padding: kPaddingAllSmall,
                decoration: BoxDecoration(
                    borderRadius: kBorderRadius,
                    color: kColorGrey.withOpacity(0.3)),
                child: Icon(EvaIcons.close),
              ).onTap(() => UiUtils.goBack())
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: YouMightAlsoLike(),
            ).paddingTop(kSpacingSmall),
          ),
          GrigoraButton(
                  onTap: () => UiUtils.goto(kRouteOrderSummary),
                  text: 'No Proceed to Check Out')
              .paddingTop(kSpacingSmall)
        ],
      ),
    );
  }
}
