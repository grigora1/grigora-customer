import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/utils/validators.dart';
import 'package:grigora/views/login_email/login_email_vm.dart';
import 'package:provider/provider.dart';

class LoginEmailScreen extends StatelessWidget {
  const LoginEmailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    return BaseWidget<LoginEmailVM>(
        model: LoginEmailVM(_formKey),
        builder: (context, model, child) {
          return SimpleLayout(
              isLoading: model.isLoading,
              body: Padding(
                padding: kPaddingAllMedium,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle('Continue with email'),
                    UIHelper.verticalSpaceLarge(),
                    _SignInForm()
                  ],
                ),
              ));
        });
  }
}

class _SignInForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<LoginEmailVM>(context);
    return Form(
      key: model.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FormItem(
            placeholder: 'Email',
            onSaved: (val) => model.email = val!,
            keyboardType: EnumKeyboardTpe.email,
            validator: (val) => Validators.validateEmail(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          FormItem(
            placeholder: 'Password',
            onSaved: (val) => model.password = val!,
            password: true,
            validator: (val) => Validators.validateText(val),
          ),
          Divider(
            color: kColorDarkGrey,
          ),
          UIHelper.verticalSpaceMedium(),
          GrigoraButton(
            onTap: () => model.doLogin(),
            text: 'Log In',
            borderRadius: kBorderRadius,
          ),
          UIHelper.verticalSpaceLarge(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _LinkText(
                text: 'Forgot Password?',
                onTap: () => kGotoScreen(context, kRouteForgotPassword),
              ),
              _LinkText(
                text: 'Create an account',
                onTap: () => kGotoScreen(context, kRouteCreateAccount),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _LinkText extends StatelessWidget {
  final String text;
  final Function() onTap;
  const _LinkText({Key? key, required this.text, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: GrigoraText(
        text,
        textColor: kColorPrimary,
      ),
    );
  }
}
