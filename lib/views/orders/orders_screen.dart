import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class OrdersScreen extends StatelessWidget {
  const OrdersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GrigoraTextTitle('Your Orders'),
        elevation: 0,
        centerTitle: true,
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            kImagesLogin,
            width: 300,
          ),
          UIHelper.verticalSpaceLarge(),
          GrigoraText('Log in to access your orders.'),
          UIHelper.verticalSpaceExtraLarge(),
          SizedBox(
            width: 200,
            child: GrigoraButton(
              onTap: () => kGotoScreen(context, kRouteLogin),
              text: 'Log in',
              borderRadius: kBorderRadius,
            ),
          )
        ],
      ),
    );
  }
}
