import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/views/track_order_single/track_order_single_screen.dart';
import 'package:grigora/ui_widgets/track_order_widget.dart';

class TrackOrderScreen extends StatelessWidget {
  const TrackOrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Track Order',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SearchWidget(),
          _TrackOrders().paddingTop(kSpacingMedium),
        ],
      ).paddingAll(kPaddingMedium),
    );
  }
}

class _TrackOrders extends StatelessWidget {
  const _TrackOrders({Key? key}) : super(key: key);

  static List<String> _tabs = ['Pending', 'Delivered', 'Cancelled'];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _tabs.length,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: TabBar(
                indicatorColor: kColorPrimary,
                tabs: UiUtils.grigoraDynamicWidget(
                    items: _tabs,
                    builder: (position) {
                      return Tab(
                          child: GrigoraTextTitle(
                        _tabs[position],
                        textSize: kFontSizeMedium,
                        textColor: position == 0 ? kColorPrimary : kColorBlack,
                      ));
                    })),
          ),
          Container(
            // height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                TabOrderSingleWidget(),
                TabOrderSingleWidget().paddingTop(kSpacingMedium),
                TabOrderSingleWidget().paddingTop(kSpacingMedium),
              ],
            ),
            // child: TabBarView(children: [
            //   Container(
            //     child: Text("Home Body"),
            //   ),
            //   Container(
            //     child: Text("Articles Body"),
            //   ),
            //   Container(
            //     child: Text("User Body"),
            //   ),
            // ]),
          ).paddingTop(kSpacingMedium),
        ],
      ),
    );
  }
}
