import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/order_summary_widget.dart';
import 'package:grigora/ui_widgets/grigora_tab_widget.dart';
import 'package:grigora/ui_widgets/additional_message.dart';

class CheckoutScreen extends StatelessWidget {
  const CheckoutScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Checkout',
      body: Container(
        padding: kPaddingAllMedium,
        child: Column(
          children: [
            GrigoraTabWidget(),
            _DeliveryAddress().paddingTop(kSpacingMedium),
            _PaymentMethod().paddingTop(kSpacingMedium),
            _ApplyCoupon().paddingTop(kSpacingMedium),
            OrderSummaryWidget().paddingTop(kSpacingMedium),
            _ActionButtons().paddingTop(kSpacingMedium)
          ],
        ).paddingBottom(kSpacingLarge),
      ),
    );
  }
}

class _ActionButtons extends StatelessWidget {
  const _ActionButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraButton(onTap: () => UiUtils.goBack(), text: 'Back to Cart')
            .expand(),
        Container(
          width: 10,
        ),
        GrigoraButton(
          onTap: () => UiUtils.showBottomSheetModal(
              context: context, widget: _ShowReceipt()),
          text: 'Check out',
          withBg: true,
        ).expand(),
      ],
    );
  }
}

class _ApplyCoupon extends StatelessWidget {
  const _ApplyCoupon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          borderRadius: kBorderRadius, border: Border.all(color: kColorGrey)),
      child: Row(
        children: [
          Expanded(
              child: FormItem(
            placeholder: 'Enter Promo Code',
            withNoBorder: true,
          )),
          GrigoraButton(
              width: 100,
              padding: kPaddingAllSmall,
              onTap: () => print('submit coupon'),
              text: 'Apply')
        ],
      ),
    );
  }
}

class _PaymentMethod extends StatelessWidget {
  const _PaymentMethod({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Payment method',
          textSize: kFontSizeMedium,
        ).paddingBottom(kSpacingSmall),
        _ItemWithChild(
          title: 'Select Option',
          onTap: () => UiUtils.showBottomSheetModal(
              context: context, widget: _SelectOption()),
        ).paddingSymmetric(vertical: kSpacingSmall),
        _ItemWithChild(
          title: 'Alergies',
          onTap: () => UiUtils.showDialog(_AddAllergy()),
        ).paddingSymmetric(vertical: kSpacingSmall),
        _AddCutlery().paddingTop(kSpacingSmall)
      ],
    );
  }
}

class _ItemWithChild extends StatelessWidget {
  final String title;
  final Function() onTap;
  const _ItemWithChild({Key? key, required this.title, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraText(
          title,
          textSize: kFontSizeMedium,
        ).expand(),
        Icon(EvaIcons.chevronRight)
      ],
    ).onTap(onTap);
  }
}

class _AddCutlery extends StatelessWidget {
  const _AddCutlery({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GrigoraText(
          'Add Cutlery',
          textSize: kFontSizeMedium,
        ).expand(),
        Switch(
          onChanged: (val) => print('hello'),
          value: true,
          activeColor: kColorWhite,
          activeTrackColor: kColorGreen,
          inactiveThumbColor: kColorWhite,
          inactiveTrackColor: kColorGrey,
        )
      ],
    );
  }
}

class _DeliveryAddress extends StatelessWidget {
  const _DeliveryAddress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Delivery Address',
          textSize: kFontSizeMedium,
        ),
        UiUtils.grigoraNetworkImageWithContainer(
                'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627407737/grigora/Object_lvipig.png',
                height: 100)
            .paddingSymmetric(vertical: kSpacingSmall),
        _ItemWithChild(
          title: 'Quegura Lounge, Lagos',
          onTap: () => UiUtils.goto(kRouteDeliveryAddress),
        ).paddingSymmetric(vertical: kSpacingSmall),
        _ItemWithChild(
          title: 'Delivery: (Now) Averagely 25mins',
          onTap: () => UiUtils.showBottomSheetModal(
              context: context, widget: _SelectDelivery()),
        ).paddingSymmetric(vertical: kSpacingSmall),
      ],
    );
  }
}

class _SelectDelivery extends StatelessWidget {
  const _SelectDelivery({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Delivery',
        ).paddingBottom(kSpacingMedium),
        _DeliveryOption(
          title: 'Order Now',
          svgImage: kImagesIconClock,
          isChecked: true,
        ).paddingSymmetric(vertical: kSpacingMedium),
        _DeliveryOption(title: 'Schedule Order', svgImage: kImagesIconSchedule)
            .paddingSymmetric(vertical: kSpacingMedium),
      ],
    ).paddingAll(kSpacingMedium);
  }
}

class _DeliveryOption extends StatelessWidget {
  final String title;
  final String svgImage;
  final bool isChecked;
  const _DeliveryOption(
      {Key? key,
      required this.title,
      this.isChecked = false,
      required this.svgImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(svgImage).paddingRight(kSpacingSmall),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              textSize: kFontSizeLarge,
            ),
          ],
        )),
        Icon(EvaIcons.checkmark).visible(isChecked),
      ],
    );
  }
}

class _AddAllergy extends StatelessWidget {
  const _AddAllergy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AdditionalMessage(
          title: 'Allergies',
        ),
        GrigoraButton(
          onTap: () => UiUtils.goBack(),
          text: 'Add',
          withBg: true,
          padding: kPaddingAllSmall,
        ).paddingTop(kSpacingSmall)
      ],
    );
  }
}

class _SelectOption extends StatelessWidget {
  const _SelectOption({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraTextTitle(
          'Payment method',
        ),
        _PaymentOptionItem(
                title: 'Card 1',
                svgImage: kImagesIconCardPayment,
                onTap: () => UiUtils.goto(kRouteCardDetails),
                subtitle: '056839280... GTBANK')
            .paddingTop(kSpacingMedium),
        _PaymentOptionItem(
                title: 'Card 2',
                svgImage: kImagesIconCardPayment,
                onTap: () => UiUtils.goto(kRouteCardDetails),
                subtitle: '056839280... GTBANK')
            .paddingTop(kSpacingMedium),
        _PaymentOptionItem(
                title: 'Pay with Cash',
                svgImage: kImagesIconCashPayment,
                onTap: () => UiUtils.showDialog(_PayWithCash()),
                subtitle: 'Py for items on Delivery')
            .paddingTop(kSpacingMedium),
      ],
    ).paddingAll(kSpacingMedium);
  }
}

class _PayWithCash extends StatelessWidget {
  const _PayWithCash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          GrigoraTextTitle('Pay with Cash'),
          Row(
            children: [
              Radio<dynamic>(
                value: 'I have the exact amount',
                groupValue: '_character',
                onChanged: (value) => print('hello'),
              ),
              GrigoraText(
                'I have the exact amount',
                textSize: kFontSizeMedium,
              ).expand(),
            ],
          ),
          Row(
            children: [
              Radio<dynamic>(
                value: 'I have the exact amount',
                groupValue: '_character',
                onChanged: (value) => print('hello'),
              ),
              GrigoraText(
                'I will required change',
                textSize: kFontSizeMedium,
              ).expand(),
            ],
          ),
          FormItem(placeholder: 'How much are you paying?'),
        ],
      ),
    );
  }
}

class _PaymentOptionItem extends StatelessWidget {
  final String title, subtitle;
  final String svgImage;
  final Function() onTap;
  const _PaymentOptionItem(
      {Key? key,
      required this.title,
      required this.svgImage,
      required this.onTap,
      required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(svgImage).paddingRight(kSpacingSmall),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraText(
              title,
              textSize: kFontSizeLarge,
            ),
            GrigoraText(subtitle).paddingTop(5)
          ],
        )),
        Icon(EvaIcons.chevronRight).paddingTop(kSpacingSmall),
      ],
    ).onTap(onTap);
  }
}

class _ShowReceipt extends StatelessWidget {
  const _ShowReceipt({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _ShowReceiptTitle(),
        _ShowReceiptItems(),
        _ShowReceiptCost(),
        GrigoraButton(
          onTap: () => UiUtils.goto(kRouteTrackOrder),
          text: 'View Order',
          withBg: true,
        ).paddingTop(kSpacingSmall),
        GrigoraButton(
          onTap: () => print('view order'),
          text: 'Download Receipt',
        ).paddingTop(kSpacingSmall)
      ],
    ).paddingAll(kSpacingMedium);
  }
}

class _ShowReceiptTitle extends StatelessWidget {
  const _ShowReceiptTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          border: Border(
            top: BorderSide(color: kColorGrey),
            left: BorderSide(color: kColorGrey),
            right: BorderSide(color: kColorGrey),
            bottom: BorderSide(
              color: kColorGrey,
            ),
          )),
      child: Row(
        children: [
          Expanded(child: GrigoraText('Receipts'.toUpperCase())),
          Row(
            children: [
              GrigoraText(
                'Cash Payment',
                textColor: kColorDarkGrey,
              ),
              SvgPicture.asset(kImagesIconNaira)
            ],
          )
        ],
      ).paddingSymmetric(horizontal: kSpacingSmall),
    );
  }
}

class _ShowReceiptItems extends StatelessWidget {
  const _ShowReceiptItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ShowReceiptItemModel> _items = [
      _ShowReceiptItemModel(title: 'Chowmein', quantity: 1, price: 180),
      _ShowReceiptItemModel(
          title: 'Handcrafter Burger', quantity: 1, price: 500, discount: 50),
      _ShowReceiptItemModel(
          title: 'Et eu sit maecenas nibh bla', quantity: 1, price: 500),
    ];
    return Container(
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          border: Border(
              left: BorderSide(color: kColorGrey),
              right: BorderSide(color: kColorGrey),
              top: BorderSide(color: kColorGrey),
              bottom: BorderSide(color: kColorGrey))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UiUtils.grigoraDynamicWidget(
            items: _items,
            builder: (position) {
              return _ShowReceiptItem(
                title: _items[position].title,
                price: _items[position].price,
                quantity: _items[position].quantity,
                discount: _items[position].discount,
                isLastItem: position == _items.length - 1,
              );
            }),
      ),
    );
  }
}

class _ShowReceiptItemModel {
  final String title;
  final int quantity;
  final double price;
  final double? discount;
  final bool isLastItem;
  _ShowReceiptItemModel(
      {this.title = 'no_title',
      this.quantity = 1,
      this.price = 0,
      this.discount,
      this.isLastItem = false});
}

class _ShowReceiptItem extends StatelessWidget {
  final String title;
  final int quantity;
  final double price;
  final double? discount;
  final bool isLastItem;
  const _ShowReceiptItem(
      {Key? key,
      required this.quantity,
      required this.price,
      this.discount,
      this.isLastItem = false,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraText(
          title,
          textSize: kFontSizeMedium,
        ).paddingSymmetric(horizontal: kSpacingSmall),
        Row(
          children: [
            Expanded(
              child: GrigoraText('$quantity x $price'),
            ),
            GrigoraText(
              '$discount',
              isLineThrough: true,
            ).paddingRight(kSpacingSmall).visible(discount != null),
            GrigoraText(
              'N${quantity * price}',
              isBoldFont: true,
              textColor: discount != null ? kColorPrimary : kColorBlack,
            )
          ],
        ).paddingTop(kSpacingSmall).paddingSymmetric(horizontal: kSpacingSmall),
        Container(
                width: kWidthFull(context),
                height: 1,
                color: kColorGrey.withOpacity(0.5))
            .paddingSymmetric(vertical: kSpacingSmall)
            .visible(!isLastItem)
      ],
    ).paddingSymmetric(vertical: 5);
  }
}

class _ShowReceiptCostModel {
  final String title;
  final double price;
  final double? discount;
  final bool isLastItem;
  final List<_ShowReceiptCostChildItemModel> childItems;
  _ShowReceiptCostModel(
      {this.title = 'no_title',
      this.price = 0,
      required this.childItems,
      this.discount,
      this.isLastItem = false});
}

class _ShowReceiptCostChildItemModel {
  final String title;
  final double percentage, price;
  _ShowReceiptCostChildItemModel(
      {this.title = '', this.price = 1, this.percentage = 0});
}

class _ShowReceiptCost extends StatelessWidget {
  const _ShowReceiptCost({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_ShowReceiptCostModel> _items = [
      _ShowReceiptCostModel(title: 'Sub Total', price: 180, childItems: [
        _ShowReceiptCostChildItemModel(
            title: 'AT + (If Applicable)', percentage: 15, price: 80),
        _ShowReceiptCostChildItemModel(title: 'Delivery Charge', price: 50)
      ]),
      _ShowReceiptCostModel(
          title: 'Gross Total',
          childItems: [
            _ShowReceiptCostChildItemModel(
                title: 'Coupon discount', percentage: 15, price: 615)
          ],
          price: 500,
          discount: 50),
      _ShowReceiptCostModel(title: 'Total Price', childItems: [], price: 678)
    ];
    return Container(
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          border: Border(
              left: BorderSide(color: kColorGrey),
              right: BorderSide(color: kColorGrey),
              top: BorderSide(color: kColorGrey),
              bottom: BorderSide(color: kColorGrey))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UiUtils.grigoraDynamicWidget(
            items: _items,
            builder: (position) {
              return _ShowReceiptCostItem(
                title: _items[position].title,
                price: _items[position].price,
                discount: _items[position].discount,
                isLastItem: position == _items.length - 1,
                childItems: _items[position].childItems,
              );
            }),
      ),
    );
  }
}

class _ShowReceiptCostItem extends StatelessWidget {
  final String title;
  final double price;
  final double? discount;
  final List<_ShowReceiptCostChildItemModel> childItems;
  final bool isLastItem;
  const _ShowReceiptCostItem(
      {Key? key,
      required this.price,
      this.discount,
      required this.childItems,
      this.isLastItem = false,
      required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            GrigoraText(
              isLastItem ? title.toUpperCase() : title,
              textSize: kFontSizeMedium,
              textColor: isLastItem ? kColorGreen : kColorBlack,
              isBoldFont: isLastItem ? true : false,
            ).expand(),
            GrigoraText(
              'N$price',
              textSize: kFontSizeMedium,
              isItalic: isLastItem ? false : true,
              isBoldFont: true,
              textColor: isLastItem ? kColorPrimary : kColorBlack,
            )
          ],
        ).paddingSymmetric(horizontal: kSpacingSmall),
        Column(
            children: UiUtils.grigoraDynamicWidget(
                items: childItems,
                builder: (position) {
                  var _item = childItems[position];
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GrigoraText('${_item.title}'),
                      GrigoraText(
                        '${_item.percentage}',
                        textColor: kColorGreen,
                      ).visible(_item.percentage > 0),
                      GrigoraText(
                        'N${_item.price}',
                      ),
                    ],
                  ).paddingTop(kSpacingSmall);
                })).paddingSymmetric(horizontal: kSpacingSmall),
        // divider line
        Container(
                width: kWidthFull(context),
                height: 1,
                color: kColorGrey.withOpacity(0.5))
            .paddingSymmetric(vertical: kSpacingSmall)
            .visible(!isLastItem)
      ],
    ).paddingSymmetric(vertical: 5);
  }
}
