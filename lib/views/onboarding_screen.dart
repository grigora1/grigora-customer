import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class OnboardingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                      kImagesRice,
                    ),
                    fit: BoxFit.cover)),
          ),
          Positioned(bottom: 0, child: _OverlayBody())
        ],
      ),
    );
  }
}

class _OverlayBody extends StatelessWidget {
  const _OverlayBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      padding: kPaddingAllMedium,
      decoration: BoxDecoration(
          color: kColorWhite,
          borderRadius: BorderRadius.vertical(top: Radius.circular(10))),
      child: Column(
        children: [
          GrigoraTextTitle(
            'Grigora works best when we know where to deliver.',
            textAlign: kTextAlignCenter,
          ),
          UIHelper.verticalSpaceLarge(),
          GrigoraText(
            'If you have your location, we can do a better job to find what you want and deiver it.',
            textAlign: kTextAlignCenter,
          ),
          UIHelper.verticalSpaceLarge(),
          GrigoraButton(
            onTap: () => Navigator.pushNamed(context, kRouteHome),
            text: 'Continue',
            borderRadius: BorderRadius.circular(10),
          )
        ],
      ),
    );
  }
}
