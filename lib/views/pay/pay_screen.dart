import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class PayScreen extends StatelessWidget {
  const PayScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GrigoraTextTitle('Grigora Pay'),
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            kImagesPay,
            width: 300,
          ),
          UIHelper.verticalSpaceLarge(),
          GrigoraText('Log in to access your Grigora Pay.'),
          UIHelper.verticalSpaceExtraLarge(),
          SizedBox(
            width: 200,
            child: GrigoraButton(
              onTap: () => kGotoScreen(context, kRouteLogin),
              text: 'Log in',
              borderRadius: kBorderRadius,
            ),
          )
        ],
      ),
    );
  }
}
