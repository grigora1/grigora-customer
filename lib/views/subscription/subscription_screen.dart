import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_nested_scroll.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/user_info_widget.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/views/payment_methods/saved_cards_screen.dart';
import 'package:nb_utils/nb_utils.dart';

class SubscriptionScreen extends StatefulWidget {
  const SubscriptionScreen({Key? key}) : super(key: key);

  @override
  State<SubscriptionScreen> createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  bool saveAddress = true;
  @override
  Widget build(BuildContext context) {
    return LayoutWithNestedScroll(
      expandedHeight: MediaQuery.of(context).size.height * 0.25,
      backgroundWidget: UiUtils.grigoraNetworkImageWithContainer(
          'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delicious-breakfast-on-a-light-table-royalty-free-image-863444442-1543345985.jpg',
          height: MediaQuery.of(context).size.height * 0.6),
      body: Form(
        child: Column(
          children: [
            RichText(
              textAlign: TextAlign.center,
              text: new TextSpan(
                style: new TextStyle(
                  fontSize: 21,
                  color: Color(0xFF343434),
                ),
                children: <TextSpan>[
                  new TextSpan(
                      text: 'Zero ',
                      style: TextStyle(
                          color: kColorPrimaryRed,
                          fontWeight: FontWeight.bold)),
                  new TextSpan(
                      text: 'delivery fees from your favorite restaurants',
                      style: new TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ).paddingTop(kSpacingLarge),
            _IconText(
                    icon: kImagesCardIcon,
                    text: 'Save an average of N100 per order')
                .paddingTop(kSpacingSmall),
            _IconText(
                    icon: kImagesAccountIcon,
                    text: 'Look for the badge to find 200 eligible vendors')
                .paddingTop(kSpacingSmall),
            _IconText(
                    icon: kImagesIconCashPayment,
                    text:
                        'Enjoy zero delivery fees and reduced service fees on orders over N1000')
                .paddingTop(kSpacingSmall),
            _IconText(
                    icon: kImagesTimingIcon,
                    text: 'N1000/month, easily cancel anytime.')
                .paddingTop(kSpacingSmall),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  SavedCardScreen(),
                );
              },
              title: 'Charge to',
              subtitle: 'MASTERCARD ...9280',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingLarge)
                .paddingBottom(kSpacingMedium),
            GrigoraButton(
              onTap: () => print('save card details'),
              text: 'Subscribe',
              withBg: true,
            ).paddingTop(kSpacingMedium)
          ],
        ).paddingSymmetric(horizontal: kSpacingMedium),
      ),
    );
  }
}

class _IconText extends StatelessWidget {
  final String icon;
  final String text;
  const _IconText({Key? key, required this.icon, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset(
            icon,
            color: kColorPrimaryRed,
            height: 20,
          ).paddingRight(kSpacingMedium),
          Expanded(
              child: GrigoraText(
            this.text,
            textColor: kColorGenericGrey,
            textAlign: TextAlign.start,
            textSize: 16,
            maxLines: 5,
          )),
        ],
      ).paddingTop(kSpacingSmall),
    );
  }
}

class _TitleSubButton extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function() onTap;
  final bool haveDivider;
  const _TitleSubButton(
      {Key? key,
      this.title = '',
      this.subtitle = '',
      required this.onTap,
      this.haveDivider = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle(
                      title,
                      textAlign: TextAlign.start,
                      textSize: 18,
                      isLightFont: true,
                    ),
                    Visibility(
                      visible: this.subtitle != '',
                      child: GrigoraText(
                        subtitle,
                        textColor: kColorGenericGrey,
                        textAlign: TextAlign.start,
                        textSize: 12,
                      ).paddingTop(kSpacingSmall),
                    )
                  ],
                ),
                Row(
                  children: [
                    GrigoraText(
                      'change',
                      textAlign: TextAlign.start,
                      textSize: 12,
                    ),
                    Icon(
                      Icons.chevron_right,
                      size: 20,
                    ),
                  ],
                )
              ],
            ),
          ],
        ).paddingTop(kSpacingSmall),
      ),
    );
  }
}
