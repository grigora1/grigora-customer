import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(kMediumPadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper.verticalSpaceLarge(),
            GrigoraText(
              'Search',
            ),
            UIHelper.verticalSpaceLarge(),
            TextFormField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  border: InputBorder.none,
                  filled: true,
                  hintText: 'Search Dzooco',
                  hintStyle: TextStyle(fontSize: kFontSizeMedium)),
              validator: (String? value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
            ),
            UIHelper.verticalSpaceLarge(),
            UIHelper.verticalSpaceLarge(),
            SearchSection(
                title: 'Top Searches',
                items: ['Tacos', 'Chick-fil-a', 'Indian', 'Chicken', 'Sushi']),
            UIHelper.verticalSpaceLarge(),
            SearchSection(
                title: 'Cuisines', items: ['Alcohol', 'Chicken', 'Sushi']),
            UIHelper.verticalSpaceLarge(),
            SearchSection(
                title: 'Top Searches',
                items: ['Tacos', 'Chick-fil-a', 'Indian', 'Chicken', 'Sushi']),
          ],
        ),
      ),
    );
  }
}

class SearchSection extends StatelessWidget {
  final String? title;
  final List<String?> items;
  SearchSection({this.title, required this.items});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GrigoraText(
          title ?? 'no_title',
        ),
        UIHelper.verticalSpaceLarge(),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: items
              .map(
                (e) => Container(
                  height: 50,
                  child: Text(e ?? 'no_text',
                      style: TextStyle(fontSize: 16, color: kColorPrimary)),
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
