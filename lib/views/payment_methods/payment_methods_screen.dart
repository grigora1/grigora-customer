import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/offer.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/card_details/card_details_screen.dart';
import 'package:grigora/views/edit_profile/edit_profile_screen.dart';
import 'package:grigora/views/home/filter_screen.dart';
import 'package:grigora/views/offers/offers_vm.dart';
import 'package:grigora/views/payment_methods/payment_methods_screen.dart';
import 'package:grigora/views/payment_methods/saved_cards_screen.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_vm.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:grigora/views/saved_address/saved_address.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/category_single_widget/special_offers.dart';
import 'package:provider/provider.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/home_widgets/order_again.dart';
import 'package:grigora/views/cart/cart_screen.dart';
import 'package:grigora/ui_widgets/rating_widget.dart';
import 'package:grigora/ui_widgets/grigora_tab_widget.dart';

class PaymentMethodsScreen extends StatelessWidget {
  const PaymentMethodsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return LayoutWithBack(
      title: 'Payment Methods',
      withStack: false,
      withShoppingCart: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  SavedCardScreen(),
                );
              },
              title: 'Saved Cards',
              subtitle: '2 cards saved',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  CardDetailsScreen(),
                );
              },
              title: 'Add new Card',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {},
              haveDivider: false,
              title: 'Grigora Credits',
              subtitle: 'Your credit value will be automatically applied to your next order',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {},
              title: '₦ 4000',
              subtitle: 'Applied only in Nigeria.',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
          ],
        ),
      ),
    );
  }
}

class _TitleSubButton extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function() onTap;
  final bool haveDivider;
  const _TitleSubButton(
      {Key? key,
      this.title = '',
      this.subtitle = '',
      required this.onTap,
      this.haveDivider = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GrigoraTextTitle(
                  title,
                  textAlign: TextAlign.start,
                  textSize: 18,
                ),
                Visibility(
                  visible: this.subtitle != '',
                  child: GrigoraText(
                    subtitle,
                    textColor: kColorGenericGrey,
                    textAlign: TextAlign.start,
                    textSize: 14,
                  ).paddingTop(kSpacingSmall),
                )
              ],
            ).paddingLeft(kSpacingMedium),
            Visibility(
              visible: haveDivider,
              child: Container(
                height: 0.2,
                decoration: BoxDecoration(color: kColorGenericGrey),
              ).paddingTop(kSpacingMedium).paddingTop(kSpacingSmall),
            )
          ],
        ).paddingTop(kSpacingSmall),
      ),
    );
  }
}

class _TextAndChevron extends StatelessWidget {
  final String title;
  final Color color;
  final bool big;
  final Function() onTap;
  const _TextAndChevron(
      {Key? key,
      this.title = '',
      this.color = Colors.black,
      this.big = true,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GrigoraTextTitle(
              title,
              textAlign: TextAlign.start,
              textSize: big ? 18 : 16,
              textColor: color,
              isLightFont: !big,
            ),
            Icon(
              Icons.chevron_right,
              color: color,
              size: 18,
            )
          ],
        ).paddingTop(kSpacingSmall).paddingLeft(kSpacingMedium),
      ),
    );
  }
}
