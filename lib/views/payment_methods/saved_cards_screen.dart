import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/card_details/card_details_screen.dart';
import 'package:grigora/views/edit_profile/edit_profile_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';

class SavedCardScreen extends StatelessWidget {
  const SavedCardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Payment Methods',
      withStack: false,
      withShoppingCart: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GrigoraTextTitle(
              'Saved Cards',
              textAlign: TextAlign.start,
              textSize: 18,
            ).paddingLeft(kSpacingMedium).paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  CardDetailsScreen(),
                );
              },
              title: 'Card 1',
              subtitle: '056839280... GTBANK',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  CardDetailsScreen(),
                );
              },
              title: 'Card 2',
              subtitle: '056839280... GTBANK',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    UiUtils.goToWidget(
                      CardDetailsScreen(),
                    );
                  },
                  child: GrigoraTextTitle(
                    'Add New Card',
                    textAlign: TextAlign.center,
                    textSize: 14,
                    textColor: kColorPrimaryRed,
                  ).paddingTop(kSpacingLarge),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _TitleSubButton extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function() onTap;
  final bool haveDivider;
  const _TitleSubButton(
      {Key? key,
      this.title = '',
      this.subtitle = '',
      required this.onTap,
      this.haveDivider = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvgPicture.asset(kImagesCardIcon)
                        .paddingRight(kSpacingSmall),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GrigoraTextTitle(
                          title,
                          textAlign: TextAlign.start,
                          textSize: 18,
                        ),
                        Visibility(
                          visible: this.subtitle != '',
                          child: GrigoraText(
                            subtitle,
                            textColor: kColorGenericGrey,
                            textAlign: TextAlign.start,
                            textSize: 14,
                          ).paddingTop(kSpacingSmall),
                        )
                      ],
                    ),
                  ],
                ),
                Icon(
                  Icons.chevron_right,
                  size: 20,
                )
              ],
            ).paddingLeft(kSpacingSmall),
            Visibility(
              visible: haveDivider,
              child: Container(
                height: 0.2,
                decoration: BoxDecoration(color: kColorGenericGrey),
              ).paddingTop(kSpacingMedium).paddingTop(kSpacingSmall),
            )
          ],
        ).paddingTop(kSpacingSmall),
      ),
    );
  }
}
