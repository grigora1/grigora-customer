import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_nested_scroll.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/user_info_widget.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';

class AddAddressScreen extends StatefulWidget {
  const AddAddressScreen({Key? key}) : super(key: key);

  @override
  State<AddAddressScreen> createState() => _AddAddressScreenState();
}

class _AddAddressScreenState extends State<AddAddressScreen> {
  bool saveAddress = true;
  @override
  Widget build(BuildContext context) {
    return LayoutWithNestedScroll(
      title: 'Add delivery address',
      expandedHeight: MediaQuery.of(context).size.height * 0.6,
      backgroundWidget: UiUtils.grigoraNetworkImageWithContainer(
          'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627473450/grigora/bg_1_garazv.png',
          height: MediaQuery.of(context).size.height * 0.6),
      body: Form(
        child: Column(
          children: [
            FormItem(
              placeholder: 'Enter Address 1',
              label: 'Address 1',
            ).paddingTop(kSpacingMedium),
            FormItem(
              placeholder: 'Enter Address 2',
              label: 'Address 2',
            ).paddingTop(kSpacingMedium),
            FormItem(
              placeholder: 'Enter Unit / Floor',
              label: 'Unit / Floor',
            ).paddingTop(kSpacingMedium),
            FormItem(
              placeholder: '+966 837379379',
              label: 'Phone Number',
            ).paddingTop(kSpacingMedium),
            Row(
              children: [
                Expanded(
                    child: GrigoraText(
                  'Save as primary delivery address',
                  isBoldFont: false,
                )),
                Switch(
                  onChanged: (val) {
                    setState(() {
                      saveAddress = val;
                    });
                  },
                  value: saveAddress,
                  activeColor: kColorWhite,
                  activeTrackColor: kColorGreen,
                  inactiveThumbColor: kColorWhite,
                  inactiveTrackColor: kColorGrey,
                ),
              ],
            ).paddingTop(kSpacingMedium),
            GrigoraButton(
              onTap: () => print('save card details'),
              text: 'Add',
              withBg: true,
            ).paddingTop(kSpacingMedium)
          ],
        ).paddingSymmetric(horizontal: kSpacingMedium),
      ),
    );
  }
}

class _TimeWidget extends StatelessWidget {
  const _TimeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: kWidthFull(context),
      padding: kPaddingAllMedium,
      color: kColorBlack,
      child: Column(
        children: [
          GrigoraTextTitle(
            '22.34',
            textColor: kColorWhite,
            textSize: 50,
          ),
          GrigoraText(
            'Estimated Delivery time',
            textColor: kColorWhite,
          ),
        ],
      ),
    );
  }
}

class _ToAndFrom extends StatelessWidget {
  const _ToAndFrom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllMedium,
      child: Column(
        children: [
          _ToAndFromItem(
            title: 'Radison blu Hotel',
            time: '25mins',
            isVendor: true,
          ),
          _ToAndFromItem(
            title: 'Quegara Lounge, Lagos',
          )
        ],
      ),
    );
  }
}

class _ToAndFromItem extends StatelessWidget {
  final String title;
  final String? time;
  final bool isVendor;
  const _ToAndFromItem(
      {Key? key, required this.title, this.isVendor = false, this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              border: isVendor
                  ? Border(left: BorderSide(color: kColorBlack))
                  : Border()),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GrigoraTextTitle(title),
                      GrigoraText('($time)').visible(time != null)
                    ],
                  ),
                  GrigoraText(isVendor ? 'Vendor Address' : 'Delivery Address')
                      .paddingTop(kSpacingSmall / 2)
                ],
              ),
              Container(
                height: 50,
                width: 1,
              ).visible(isVendor)
            ],
          ).paddingLeft(kSpacingLarge),
        ).paddingLeft(kSpacingMedium),
        Positioned(
          left: 0,
          child: Container(
            padding: kPaddingAllSmall,
            decoration: BoxDecoration(
                color: kColorGreyLight, borderRadius: kBorderRadius),
            child:
                SvgPicture.asset(isVendor ? kImagesIconStart : kImagesIconEnd),
          ),
        ),
      ],
    );
  }
}
