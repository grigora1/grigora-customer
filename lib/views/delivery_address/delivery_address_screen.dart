import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_nested_scroll.dart';

class DeliveryAddressScreen extends StatelessWidget {
  const DeliveryAddressScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithNestedScroll(
        title: 'Delivery Address',
        expandedHeight: MediaQuery.of(context).size.height / 1.8,
        backgroundWidget: Container(
          decoration: BoxDecoration(
              image: UiUtils.grigoraNetworkImageProvider(
                  'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627432324/grigora/bg_nk4n83.png')),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [_FormWidget()],
        ).paddingAll(kSpacingMedium));
  }
}

class _FormWidget extends StatelessWidget {
  const _FormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormItem(
            placeholder: 'Enter value',
            label: 'Address',
            // onSaved: (val) => model.password = val!,
            // validator: (val) => Validators.validateText(val),
          ),
          FormItem(
            placeholder: 'Enter value',
            label: 'Unit/Floor',
            // onSaved: (val) => model.password = val!,
            // validator: (val) => Validators.validateText(val),
          ).paddingTop(kSpacingMedium),
          FormItem(
            placeholder: 'Enter value',
            label: 'Phone Number',
            // onSaved: (val) => model.password = val!,
            // validator: (val) => Validators.validateText(val),
          ).paddingTop(kSpacingMedium),
          Row(
            children: [
              Expanded(child: GrigoraText('Save as primary delivery address')),
              Switch(
                onChanged: (val) => print('hello'),
                value: true,
                activeColor: kColorWhite,
                activeTrackColor: kColorGreen,
                inactiveThumbColor: kColorWhite,
                inactiveTrackColor: kColorGrey,
              ),
            ],
          ),
          GrigoraButton(
            text: 'Add',
            onTap: () => UiUtils.goBack(),
            withBg: true,
          ).paddingSymmetric(vertical: kSpacingLarge),
        ],
      ),
    );
  }
}
