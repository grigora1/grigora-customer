import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/models/args.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/category_single/category_single_vm.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/category_single_widget/categories_widget.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/category_single_widget/popular_restaurants.dart';
import 'package:grigora/ui_widgets/category_single_widget/food_vendor_nearby.dart';
import 'package:grigora/ui_widgets/category_single_widget/popular_cuisines.dart';
import 'package:grigora/ui_widgets/category_single_widget/special_offers.dart';
import 'package:grigora/ui_widgets/category_single_widget/restaurant_nearby.dart';
import 'package:grigora/ui_widgets/home_widgets/slider_widget.dart';
import 'package:grigora/ui_widgets/category_single_widget/food_items.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';

class CategorySingleScreen extends StatelessWidget {
  final ArgsCategory item;
  const CategorySingleScreen({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CategorySingleVM>(
        model: CategorySingleVM(),
        builder: (context, model, child) {
          return LayoutWithBack(
            title: item.title,
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _SearchSection().paddingAll(kSpacingMedium),
                SliderWidget().paddingBottom(kSpacingLarge),
                CategoriesWidget().paddingBottom(kSpacingLarge),
                Visibility(
                    visible: !item.title.contains('Restaurant'),
                    child: Column(
                      children: [
                        FilterWidget()
                            .paddingBottom(kSpacingLarge)
                            .paddingSymmetric(horizontal: kSpacingMedium),
                        PopularRestaurants().paddingBottom(kSpacingLarge),
                        FoodVendorNearby().paddingBottom(kSpacingLarge),
                        SliderWidget().paddingBottom(kSpacingLarge),
                        PopularCuisines().paddingBottom(kSpacingLarge),
                        SpecialOffers().paddingBottom(kSpacingLarge),
                        FoodItems().paddingBottom(kSpacingLarge),
                      ],
                    )),
                Visibility(
                    visible: item.title.contains('Restaurant'),
                    child: Column(
                      children: [
                        SpecialOffers().paddingBottom(kSpacingLarge),
                        PopularRestaurants().paddingBottom(kSpacingLarge),
                        RestaurantNearby().paddingBottom(kSpacingLarge),
                        SliderWidget().paddingBottom(kSpacingLarge),
                        FoodItems().paddingBottom(kSpacingLarge),
                      ],
                    )),
              ],
            ),
          );
        });
  }
}

class _SearchSection extends StatelessWidget {
  const _SearchSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: kPaddingAllSmall,
      decoration: BoxDecoration(
          color: kColorWhite,
          border: Border.all(color: kColorBlack.withOpacity(0.1)),
          borderRadius: kBorderRadius),
      child: Row(
        children: [
          Icon(
            EvaIcons.searchOutline,
            color: kColorDarkGrey.withOpacity(0.5),
          ),
          UIHelper.horizontalSpaceMedium(),
          Expanded(
            child: FormItem(
              placeholder: 'Search for food',
            ),
          ),
          Icon(
            EvaIcons.micOutline,
            color: kColorPrimary,
          )
        ],
      ),
    );
  }
}
