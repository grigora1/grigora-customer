import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/models/args.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/layouts/simple_layout.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/utils/validators.dart';
import 'package:grigora/views/verify_otp/verify_otp_vm.dart';
import 'package:provider/provider.dart';

class VerifyOTP extends StatelessWidget {
  final ArgsVerifyOTP argsOTP;
  const VerifyOTP({Key? key, required this.argsOTP}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    return BaseWidget<VerifyOTPVM>(
        // onInitState: (model) => model.startTimer(),
        model:
            VerifyOTPVM(_formKey, token: argsOTP.token, phone: argsOTP.phone),
        builder: (context, model, child) {
          return SimpleLayout(
              isLoading: model.isLoading,
              body: Padding(
                padding: kPaddingAllMedium,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GrigoraTextTitle('OTP Verification'),
                    UIHelper.verticalSpaceMedium(),
                    GrigoraText(
                        'We have sent an OTP number to your email address ${argsOTP.email}'),
                    UIHelper.verticalSpaceLarge(),
                    _Form()
                  ],
                ),
              ));
        });
  }
}

class _Form extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<VerifyOTPVM>(context);
    return Form(
      key: model.formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SingleChildScrollView(
              child: Container(
                  child: FormItemOTP(
            callback: (val) => model.otpCode = val,
            validator: (_) => Validators.validateText(model.otpCode),
          ))),
          UIHelper.verticalSpaceLarge(),
          GrigoraButton(
            onTap: () => model.doValidateOTP(),
            text: 'Continue',
            borderRadius: kBorderRadius,
          ),
          UIHelper.verticalSpaceLarge(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GrigoraText('OTP Expires in ${model.otpExpiry}'),
              _ResendOTPButton()
            ],
          )
        ],
      ),
    );
  }
}

class _ResendOTPButton extends StatelessWidget {
  const _ResendOTPButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = Provider.of<VerifyOTPVM>(context);
    return InkWell(
        onTap: () => model.doResendOTP(),
        child: GrigoraText('Resend OTP', textColor: kColorPrimary));
  }
}
