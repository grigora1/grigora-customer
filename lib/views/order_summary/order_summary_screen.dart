import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/additional_message.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/order_summary_widget.dart';

class OrderSummaryScreen extends StatelessWidget {
  const OrderSummaryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Order Summary',
      withStack: true,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Padding(
              padding: kPaddingAllMedium,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _SummaryItems(),
                    AdditionalMessage().paddingTop(kSpacingMedium),
                    OrderSummaryWidget().paddingTop(kSpacingMedium)
                  ],
                ).paddingBottom(kSpacingExtraLarge),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: GrigoraButtonExtended(
                      title: 'Checkout', onTap: () => UiUtils.goto(kRouteLogin))
                  .paddingAll(kSpacingMedium),
            )
          ],
        ),
      ),
    );
  }
}

class _SummaryItems extends StatelessWidget {
  const _SummaryItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_SummeryItemModel> _items = [
      _SummeryItemModel(
          title: 'Radisson BLU',
          desc: '4 Items from this store',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627393051/grigora/image_3_kk1mwv.png',
          price: 134500),
      _SummeryItemModel(
          title: 'Burger King',
          desc: '7 Items from this store',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627393047/grigora/image_3_1_mqrrgb.png',
          price: 134500),
      _SummeryItemModel(
          title: 'Shoprite',
          desc: '2 Items from this store',
          image:
              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627393043/grigora/image_3_2_wpjsy3.png',
          price: 34500),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: UiUtils.grigoraDynamicWidget(
          items: _items,
          builder: (position) {
            return _SummaryItem(
              title: _items[position].title,
              desc: _items[position].desc,
              image: _items[position].image,
              price: _items[position].price,
            );
          }),
    );
  }
}

class _SummeryItemModel {
  final String image, title, desc;
  final double price;
  _SummeryItemModel(
      {required this.image,
      required this.title,
      required this.desc,
      required this.price});
}

class _SummaryItem extends StatelessWidget {
  final String image, title, desc;
  final double price;
  const _SummaryItem(
      {Key? key,
      required this.image,
      required this.price,
      required this.title,
      required this.desc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kColorWhite,
        border: Border(left: BorderSide(color: kColorPrimary, width: 3)),
        boxShadow: [kBoxShadow(kColorGrey)],
      ),
      child: Row(
        children: [
          UiUtils.grigoraNetworkImageWithContainer(image, width: 80, height: 80)
              .paddingRight(kSpacingSmall),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GrigoraTextTitle(
                  title,
                  textSize: kFontSizeMedium,
                ),
                GrigoraText(desc),
                GrigoraText('N$price'),
              ],
            ),
          ),
          Icon(EvaIcons.chevronDown).paddingRight(kSpacingSmall)
        ],
      ),
    ).paddingBottom(kPaddingSmall);
  }
}
