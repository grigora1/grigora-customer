import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:nb_utils/nb_utils.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kColorWhite,
        title: _AppBarTitle(),
        iconTheme: IconThemeData(color: kColorBlack),
        actions: [
          _SearchIcon(
            onTap: () => kGotoScreen(context, kRouteRestaurants),
          )
        ],
      ),
      drawer: _GrigoraDrawer(),
      body: Padding(
        padding: kPaddingAllMedium,
        child: SingleChildScrollView(
          child: Column(
            children: [
              _LoginInfo(),
              UIHelper.verticalSpaceLarge(),
              GrigoraButton(
                onTap: () => kGotoScreen(context, kRouteRestaurants),
                text: 'View all restaurants',
                borderRadius: kBorderRadius,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _LoginInfo extends StatelessWidget {
  const _LoginInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: kBorderRadius,
      ),
      child: ClipRRect(
        borderRadius: kBorderRadius,
        child: Column(
          children: [
            _LoginInfoContent().paddingBottom(kPaddingLarge),
            GrigoraButton(
              onTap: () => kGotoScreen(context, kRouteLogin),
              text: 'Log in',
              withBg: true,
            )
          ],
        ),
      ),
    );
  }
}

class _LoginInfoContent extends StatelessWidget {
  const _LoginInfoContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: kPaddingAllMedium,
      color: kColorPrimary.withOpacity(0.1),
      child: Row(
        children: [
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                color: kColorPrimary, borderRadius: kFullBorderRadius),
            child: Icon(
              EvaIcons.personOutline,
              color: kColorWhite,
            ),
          ).paddingRight(kPaddingSmall),
          Expanded(
              child: GrigoraText(
                  'Log in or create an account to receive rewards and offers.'))
        ],
      ),
    );
  }
}

class _DrawerItem {
  final String text;
  final String? routeName;
  final IconData icon;

  _DrawerItem({required this.text, this.routeName, required this.icon});
}

class _GrigoraDrawer extends StatelessWidget {
  final List<_DrawerItem> _items = [
    _DrawerItem(
        icon: EvaIcons.homeOutline, text: 'Home', routeName: kRouteHome),
    _DrawerItem(
      icon: EvaIcons.pricetagsOutline,
      text: 'Offers',
    ),
    _DrawerItem(
        icon: Icons.notifications_outlined,
        text: 'Notifications',
        routeName: kRouteNotifications),
    _DrawerItem(
        icon: Icons.archive_outlined,
        text: 'Your orders',
        routeName: kRouteOrders),
    _DrawerItem(
        icon: Icons.wallet_giftcard_outlined,
        text: 'Grigora Pay',
        routeName: kRoutePay),
    _DrawerItem(
        icon: Icons.settings_outlined,
        text: 'Settings',
        routeName: kRouteSettings),
    _DrawerItem(
        icon: Icons.help_center_outlined,
        text: 'Get help',
        routeName: kRouteHelp),
    _DrawerItem(
        icon: Icons.info_outline, text: 'About app', routeName: kRouteAbout),
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
          Expanded(
              child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: ListView.separated(
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 30,
                        );
                      },
                      itemCount: _items.length,
                      itemBuilder: (context, position) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.pushNamed(context,
                                _items[position].routeName ?? kRouteHome);
                            // _items
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(_items[position].icon, color: kColorBlack),
                              UIHelper.horizontalSpaceMedium(),
                              GrigoraText(_items[position].text, textSize: 16),
                            ],
                          ),
                        );
                      })))
        ])));
  }
}

class _AppBarTitle extends StatelessWidget {
  const _AppBarTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GrigoraText('Delivering to'),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GrigoraTextTitle(
              'Select your Area',
              textColor: kColorPrimary,
            ),
            Icon(
              EvaIcons.chevronDown,
              color: kColorPrimary,
            )
          ],
        )
      ],
    );
  }
}

class _SearchIcon extends StatelessWidget {
  final Function() onTap;
  const _SearchIcon({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: kPaddingAllSmall,
        child: Icon(EvaIcons.search),
      ),
    );
  }
}
