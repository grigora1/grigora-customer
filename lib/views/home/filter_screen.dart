import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:filter_list/filter_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class FilterScreen extends StatefulWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  double currentDistance = 15;

  List<String> sortBy = [
    'Featured',
    'Nearest',
    'Special Offers',
    'Recommended',
    'Free Deliver'
  ];

  List<String> types = [
    'Restaurant',
    'Grocery',
    'Pharmacy',
    'Fashion',
    'Toys',
    'Electronics'
  ];

  int rating = 4;
  int price = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
        centerTitle: true,
        elevation: 0,
        title: GrigoraTextTitle('Filter'),
      ),
      body: SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text('Hello'),
            // Text('hi'),
            _Item(
              text: 'Distance',
              side: '${currentDistance.toInt()} Km.',
              child: SfSlider(
                stepSize: 1,
                min: 0.0,
                overlayShape: SfOverlayShape(),
                max: kMaximumFilterDistance,
                value: currentDistance,
                interval: 1,
                showTicks: false,
                showLabels: false,
                enableTooltip: true,
                tooltipShape: SfPaddleTooltipShape(),
                thumbIcon: Icon(
                  Icons.circle,
                  size: 10,
                  color: Colors.white,
                ),
                minorTicksPerInterval: 1,
                onChanged: (dynamic value) {
                  setState(() {
                    currentDistance = value;
                  });
                },
              ),
            ),
            _Item(
              text: 'Sort by',
              // child: Container(),
              child: Container(
                height: 150,
                child: FilterListWidget<String>(
                  listData: sortBy,
                  hideHeaderText: true,
                  hideSearchField: true,
                  backgroundColor: Color(0x00000000),
                  onApplyButtonClick: (t) {},
                  closeIconColor: Color(0x00000000),
                  headerTextColor: Color(0x0000000),
                  headerTextStyle: TextStyle(color: Color(0x00000000)),
                  height: 120,
                  hideHeader: true,
                  unselectedChipTextStyle: TextStyle(color: Color(0xFF666666)),
                  hideSelectedTextCount: true,
                  hideCloseIcon: true,
                  // applyButonTextBackgroundColor: ,
                  applyButtonTextStyle: TextStyle(color: Color(0x00000000)),
                  controlButtonTextStyle: TextStyle(color: Color(0x00000000)),
                  applyButonTextBackgroundColor: Color(0x00000000),
                  borderRadius: 10,
                  selectedTextBackgroundColor: Color(0xFFD73032),
                  enableOnlySingleSelection: true,

                  // onApplyButtonClick: (list) {
                  //   if (list != null) {
                  //     print("Selected items count: ${list!.length}");
                  //   }
                  // },
                  choiceChipLabel: (item) {
                    /// Used to print text on chip
                    return item;
                  },
                  validateSelectedItem: (list, val) {
                    ///  identify if item is selected or not
                    return list!.contains(val);
                  },
                  onItemSearch: (list, text) {
                    /// When text change in search text field then return list containing that text value
                    ///
                    ///Check if list has value which matchs to text
                    if (list!.any((element) =>
                        element.toLowerCase().contains(text.toLowerCase()))) {
                      /// return list which contains matches
                      return list
                          .where((element) => element
                              .toLowerCase()
                              .contains(text.toLowerCase()))
                          .toList();
                    } else {
                      return [];
                    }
                  },
                ),
              ),
            ),
            _Item(
              text: 'Types',
              // child: Container(),
              child: Container(
                height: 150,
                child: FilterListWidget<String>(
                  listData: types,
                  hideHeaderText: true,
                  hideSearchField: true,
                  backgroundColor: Color(0x00000000),
                  onApplyButtonClick: (t) {},
                  closeIconColor: Color(0x00000000),
                  headerTextColor: Color(0x0000000),
                  headerTextStyle: TextStyle(color: Color(0x00000000)),
                  height: 120,
                  hideHeader: true,
                  unselectedChipTextStyle: TextStyle(color: Color(0xFF666666)),
                  hideSelectedTextCount: true,
                  hideCloseIcon: true,
                  // applyButonTextBackgroundColor: ,
                  applyButtonTextStyle: TextStyle(color: Color(0x00000000)),
                  controlButtonTextStyle: TextStyle(color: Color(0x00000000)),
                  applyButonTextBackgroundColor: Color(0x00000000),
                  borderRadius: 10,
                  selectedTextBackgroundColor: Color(0xFFD73032),
                  enableOnlySingleSelection: true,

                  // onApplyButtonClick: (list) {
                  //   if (list != null) {
                  //     print("Selected items count: ${list!.length}");
                  //   }
                  // },
                  choiceChipLabel: (item) {
                    /// Used to print text on chip
                    return item;
                  },
                  validateSelectedItem: (list, val) {
                    ///  identify if item is selected or not
                    return list!.contains(val);
                  },
                  onItemSearch: (list, text) {
                    /// When text change in search text field then return list containing that text value
                    ///
                    ///Check if list has value which matchs to text
                    if (list!.any((element) =>
                        element.toLowerCase().contains(text.toLowerCase()))) {
                      /// return list which contains matches
                      return list
                          .where((element) => element
                              .toLowerCase()
                              .contains(text.toLowerCase()))
                          .toList();
                    } else {
                      return [];
                    }
                  },
                ),
              ),
            ),
            _Item(
              text: 'Ratings',
              // child: Container(),
              child: Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            rating = 0;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: rating == 0
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(rating == 0
                                  ? kImagesStarWhiteIcon
                                  : kImagesStarGoldIcon),
                              SizedBox(
                                width: 10,
                              ),
                              GrigoraText(
                                '1',
                                textColor: rating == 0
                                    ? Color(0xFFFFFFFF)
                                    : Color(0xFF666666),
                              )
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            rating = 1;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: rating == 1
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(rating == 1
                                  ? kImagesStarWhiteIcon
                                  : kImagesStarGoldIcon),
                              SizedBox(
                                width: 10,
                              ),
                              GrigoraText(
                                '2',
                                textColor: rating == 1
                                    ? Color(0xFFFFFFFF)
                                    : Color(0xFF666666),
                              )
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            rating = 2;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: rating == 2
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(rating == 2
                                  ? kImagesStarWhiteIcon
                                  : kImagesStarGoldIcon),
                              SizedBox(
                                width: 10,
                              ),
                              GrigoraText(
                                '3',
                                textColor: rating == 2
                                    ? Color(0xFFFFFFFF)
                                    : Color(0xFF666666),
                              )
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            rating = 3;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: rating == 3
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(rating == 3
                                  ? kImagesStarWhiteIcon
                                  : kImagesStarGoldIcon),
                              SizedBox(
                                width: 10,
                              ),
                              GrigoraText(
                                '4',
                                textColor: rating == 3
                                    ? Color(0xFFFFFFFF)
                                    : Color(0xFF666666),
                              )
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            rating = 4;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: rating == 4
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(rating == 4
                                  ? kImagesStarWhiteIcon
                                  : kImagesStarGoldIcon),
                              SizedBox(
                                width: 10,
                              ),
                              GrigoraText(
                                '5',
                                textColor: rating == 4
                                    ? Color(0xFFFFFFFF)
                                    : Color(0xFF666666),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
            UIHelper.verticalSpaceSmall(),
            _Item(
              text: 'Price',
              // child: Container(),
              child: Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            price = 0;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: price == 0
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(kImagesPrice1,
                                  color: price == 0
                                      ? Color(0xFFFFFFFF)
                                      : Color(0xFF666666)),
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            price = 1;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: price == 1
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(kImagesPrice2,
                                  color: price == 1
                                      ? Color(0xFFFFFFFF)
                                      : Color(0xFF666666)),
                            ],
                          ),
                        ),
                      ),
                      UIHelper.horizontalSpaceMedium(),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            price = 2;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: price == 2
                                  ? Color(0xFFD73032)
                                  : Color(0xFFFCFCFC)),
                          child: Row(
                            children: [
                              SvgPicture.asset(kImagesPrice3,
                                  color: price == 2
                                      ? Color(0xFFFFFFFF)
                                      : Color(0xFF666666)),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
            UIHelper.verticalSpaceMedium(),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: GrigoraButton(
                  onTap: () {},
                  text: 'Apply Filter',
                  withBg: true,
                )),

            UIHelper.verticalSpaceMedium(),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: GrigoraButton(
                  onTap: () {},
                  text: 'Reset',
                  withBg: false,

                )),
                UIHelper.verticalSpaceExtraLarge(),
          ],
        ),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final String text;
  final Widget child;
  final String side;
  const _Item(
      {Key? key, required this.text, required this.child, this.side = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kColorWhite,
      padding: kPaddingAllMedium,
      margin: EdgeInsets.only(bottom: 1),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GrigoraText(
                text,
                isBoldFont: true,
                textSize: 16,
              ),
              GrigoraText(
                side,
                textColor: kColorPrimary,
                isBoldFont: true,
              ),
            ],
          ),
          UIHelper.verticalSpaceMedium(),
          child
        ],
      ),
    );
  }
}
