import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';

class CardDetailsScreen extends StatelessWidget {
  const CardDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Card details',
      body: Column(
        children: [
          Row(
            children: [
              GrigoraTextTitle('Enter card Details').expand(),
              Icon(EvaIcons.flip2Outline),
            ],
          ),
          Image.asset(
            kImagesCard,
            width: kWidthFull(context),
          ).paddingTop(kSpacingMedium),
          _FormWidget().paddingTop(kSpacingMedium)
        ],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _FormWidget extends StatelessWidget {
  const _FormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          FormItem(
            placeholder: 'Enter value',
            label: 'Card number',
          ),
          Row(
            children: [
              FormItem(
                placeholder: 'Enter value',
                label: 'Expiry date',
              ).paddingRight(kSpacingSmall).expand(),
              FormItem(
                placeholder: 'Enter value',
                label: 'CVV',
              ).paddingLeft(kSpacingSmall).expand()
            ],
          ).paddingTop(kSpacingMedium),
          Row(
            children: [
              Expanded(
                  child: GrigoraText(
                'Save card details',
                isBoldFont: true,
              )),
              Switch(
                onChanged: (val) => print('hello'),
                value: false,
                activeColor: kColorWhite,
                activeTrackColor: kColorGreen,
                inactiveThumbColor: kColorWhite,
                inactiveTrackColor: kColorGrey,
              ),
            ],
          ).paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => print('save card details'),
            text: 'Done',
            withBg: true,
          ).paddingTop(kSpacingLarge)
        ],
      ),
    );
  }
}
