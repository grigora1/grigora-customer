import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/ui_helper.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kColorGrey,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kColorBlack),
        backgroundColor: kColorWhite,
        centerTitle: true,
        elevation: 0,
        title: GrigoraTextTitle('Settings'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _Item(
              text: 'Notifications',
              value: 'Enabled',
              onTap: () => print('tapping this one'),
            ),
            UIHelper.verticalSpaceMedium(),
            _Item(
              text: 'Language',
              value: 'English',
              onTap: () => print('tapping this one'),
            ),
            _Item(
              text: 'Country',
              value: 'Nigeria',
              onTap: () => kGotoScreen(context, kRouteCountrySelect),
            ),
            UIHelper.verticalSpaceMedium(),
            _Item(
              text: 'Log in',
              hideIcon: true,
              onTap: () => print('tapping this one'),
            ),
          ],
        ),
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final String text;
  final String value;
  final Function() onTap;
  final bool hideIcon;
  const _Item(
      {Key? key,
      required this.text,
      required this.onTap,
      this.value = "",
      this.hideIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: kColorWhite,
        padding: kPaddingAllMedium,
        margin: EdgeInsets.only(bottom: 1),
        child: Row(
          children: [
            Expanded(
                child: GrigoraText(
              text,
              isBoldFont: true,
            )),
            GrigoraText(value),
            Visibility(
              visible: !hideIcon,
              child: Icon(
                EvaIcons.chevronRight,
                color: kColorPrimary,
              ),
            )
          ],
        ),
      ),
    );
  }
}
