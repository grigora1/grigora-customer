import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/utils/base_model.dart';

class RestaurantSingleVM extends BaseModel {
  final RestaurantWidgetModel item;
  RestaurantSingleVM(this.item);
}
