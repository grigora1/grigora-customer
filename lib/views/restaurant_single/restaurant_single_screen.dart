import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_vm.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/category_single_widget/special_offers.dart';
import 'package:provider/provider.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/home_widgets/order_again.dart';
import 'package:grigora/views/cart/cart_screen.dart';
import 'package:grigora/ui_widgets/rating_widget.dart';
import 'package:grigora/ui_widgets/grigora_tab_widget.dart';

class RestaurantSingleScreen extends StatelessWidget {
  final RestaurantWidgetModel item;
  const RestaurantSingleScreen({Key? key, required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return BaseWidget<RestaurantSingleVM>(
        model: RestaurantSingleVM(item),
        builder: (context, model, child) {
          return LayoutWithBack(
            title: item.title,
            withStack: true,
            body: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SearchWidget()
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingSmall),
                      Stack(
                        children: [
                          _BannerWidget()
                              .paddingSymmetric(horizontal: kSpacingMedium)
                              .paddingTop(kSpacingMedium)
                              .paddingBottom(kSpacingMedium),
                          Positioned(
                              left: width * 0.08,
                              bottom: 0,
                              child: _ProfilePicWidget())
                        ],
                      ),
                      _TitleWidget()
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingSmall),
                      _AddressWidget()
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingMedium),
                      _TimingWidget()
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingSmall),
                      // _CategoriesWidget()
                      Row(
                        children: [
                          RatingWidget().paddingRight(kSpacingSmall),
                          _IconTextWidget(
                            picture: SvgPicture.asset(kImagesTimingIcon),
                            text: '32 Mins',
                            textColor: Color(0xFF666666),
                          ).paddingRight(kSpacingSmall),
                          _IconTextWidget(
                            picture: SvgPicture.asset(kImagesBikeIcon),
                            text: 'NGN 0',
                            textColor: Color(0xFF666666),
                          )
                        ],
                      )
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingSmall),
                      GrigoraTabWidget()
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingMedium),
                      GrigoraNetworkImage(
                              'https://res.cloudinary.com/dbdpkkfaf/image/upload/v1627306532/grigora/Frame_1060_1_olacqr.png')
                          .paddingSymmetric(horizontal: kSpacingMedium)
                          .paddingTop(kSpacingMedium),
                      _PopularCategories().paddingTop(kSpacingLarge),
                      OrderAgain().paddingTop(kSpacingLarge),
                      SpecialOffers(
                        width: kWidthFull(context) * 0.8,
                      ).paddingTop(kSpacingLarge),
                      VendorCategories(
                        title: 'Menu',
                      ).paddingTop(kSpacingLarge),
                    ],
                  ),
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: GrigoraButtonExtended(
                      title: 'My Orders',
                      onTap: () => print('see my orders'),
                      rightWidget: Container(
                        padding: kPaddingAllSmall,
                        decoration: BoxDecoration(
                            borderRadius: kFullBorderRadius,
                            color: kColorWhite.withOpacity(0.3)),
                        child: GrigoraText(
                          '0',
                          textColor: kColorWhite,
                        ),
                      ).paddingRight(kSpacingSmall),
                    ).paddingAll(kSpacingMedium))
              ],
            ),
          );
        });
  }
}

class _IconTextWidget extends StatelessWidget {
  final SvgPicture picture;
  final String text;
  final Color textColor;
  const _IconTextWidget({Key? key, required this.picture, required this.text, required this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        picture.paddingRight(kSpacingSmall),
        GrigoraText(
          text,
          textColor: textColor,
        )
      ],
    );
  }
}

class _BannerWidget extends StatelessWidget {
  const _BannerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;
    return Container(
      width: kWidthFull(context),
      height: 200,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover, image: NetworkImage(_restaurant.image))),
    );
  }
}

class _ProfilePicWidget extends StatelessWidget {
  const _ProfilePicWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;
    return Container(
      width: width * 0.15,
      height: width * 0.15,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 2),
          image: DecorationImage(
              image: NetworkImage(_restaurant.profilePic), fit: BoxFit.cover),
          color: Colors.redAccent,
          borderRadius: BorderRadius.all(Radius.circular(width))),
    );
  }
}

class _TitleWidget extends StatelessWidget {
  const _TitleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;
    return Row(
      children: [
        Expanded(
          child: Row(
            children: [
              GrigoraTextTitle(
                _restaurant.title,
                isLightFont: false,
              ).paddingRight(kSpacingSmall),
              SvgPicture.asset(kImagesIconVerified)
            ],
          ),
        ),
        SvgPicture.asset(kImagesIconChat)
      ],
    );
  }
}

class _AddressWidget extends StatelessWidget {
  const _AddressWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;
    return GrigoraText(
      _restaurant.address,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      isBoldFont: false,
      textColor: Color(0xFF666666),
    );
  }
}

class _TimingWidget extends StatelessWidget {
  const _TimingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;
    return Row(
      children: [
        SvgPicture.asset(kImagesTimingIcon),
        UIHelper.horizontalSpaceSmall(),
        GrigoraText(
          _restaurant.workingHours,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          isBoldFont: false,
          textColor: Color(0xFF666666),
        ),
        UIHelper.horizontalSpaceLarge(),
        SvgPicture.asset(kImagesDot, color: kColorPrimaryRed,),
        UIHelper.horizontalSpaceMedium(),
        GrigoraText(
          _restaurant.open ? 'OPEN' : 'CLOSED',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          isBoldFont: false,
          textColor: kColorPrimaryRed,
        ),
      ],
    );
  }
}

class _RestaurantInfo extends StatelessWidget {
  const _RestaurantInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var model = Provider.of<RestaurantSingleVM>(context);
    var _restaurant = model.item;

    return Padding(
      padding: kPaddingAllMedium,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UIHelper.verticalSpaceMedium(),
          _RestaurantInfoItem(
            icon: EvaIcons.carOutline,
            title: 'Delivery Time',
            value: _restaurant.deliveryTime,
          ),
          _RestaurantInfoItem(
            icon: EvaIcons.starOutline,
            title: 'Rating',
            value: _restaurant.rating,
          ),
          _RestaurantInfoItem(
            icon: EvaIcons.pricetagsOutline,
            title: 'Discount',
            value: _restaurant.discount,
            isLastItem: true,
          ),
        ],
      ),
    );
  }
}

class _RestaurantInfoItem extends StatelessWidget {
  final IconData icon;
  final String title, value;
  final bool isLastItem;
  const _RestaurantInfoItem(
      {Key? key,
      required this.icon,
      this.isLastItem = false,
      required this.title,
      required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: kSmallPadding),
          child: Row(
            children: [
              Icon(icon),
              UIHelper.horizontalSpaceMedium(),
              GrigoraText(
                title,
                isBoldFont: true,
              ),
              UIHelper.horizontalSpaceMedium(),
              Expanded(
                child: GrigoraText(
                  value,
                ),
              ),
            ],
          ),
        ),
        Visibility(
            visible: !isLastItem,
            child: Container(
              height: 1,
              color: kColorGrey,
            ))
      ],
    );
  }
}

class _PopularCategories extends StatelessWidget {
  const _PopularCategories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<_CategoryWidgetModel> _items = [
      _CategoryWidgetModel(text: 'Filter', isSpecial: true),
      _CategoryWidgetModel(text: 'All'),
      _CategoryWidgetModel(text: 'Burger'),
      _CategoryWidgetModel(text: 'Pizza'),
      _CategoryWidgetModel(text: 'Noodle'),
      _CategoryWidgetModel(text: 'Chicken'),
      _CategoryWidgetModel(text: 'French Fries'),
      _CategoryWidgetModel(text: 'Chocolates'),
      _CategoryWidgetModel(text: 'Spaghetti'),
    ];
    return GrigoraHorizontalScroll<_CategoryWidgetModel>(
      items: _items,
      builder: (int position, _CategoryWidgetModel model) =>
          _PopularCategoriesItem(
        text: model.text,
        isSpecial: model.isSpecial,
        isSelected: position == 1,
      ),
    );
  }
}

class _CategoryWidgetModel extends HorizontalScrollModel {
  final String text;
  final bool isSpecial;
  _CategoryWidgetModel({required this.text, this.isSpecial = false});
}

class _PopularCategoriesItem extends StatelessWidget {
  final String text;
  final bool isSelected, isSpecial;
  const _PopularCategoriesItem(
      {Key? key,
      this.isSelected = false,
      this.isSpecial = false,
      required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: kSmallPadding),
      padding: EdgeInsets.symmetric(
          horizontal: kSpacingMedium, vertical: kSpacingSmall),
      decoration: BoxDecoration(
          borderRadius: kBorderRadius,
          // boxShadow: [kBoxShadow(kColorGrey)],
          color: isSpecial
              ? kColorWhite
              : isSelected
                  ? kColorGold.withOpacity(0.1)
                  : kColorWhite),
      child: isSpecial
          ? Icon(EvaIcons.options)
          : GrigoraText(
              text,
              textSize: kFontSizeMedium,
            ),
    );
  }
}

class VendorCategories extends StatelessWidget {
  final String? title;
  const VendorCategories({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<VendorWidgetModel> _items = [
      VendorWidgetModel(
          title: 'Burgers',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Pizzas',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1573555957315-723d970bcdde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Chicken',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1572695064956-52bfdd19cfee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      VendorWidgetModel(
          title: 'Drinks',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1422919869950-5fdedb27cde8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    ];

    return GrigoraSectionWidget(
        title: title ?? 'Restaurant Nearby',
        showAllAction: () => print('go somewhere'),
        child: ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, position) {
                  return VendorItem(
                      title: _items[position].title,
                      time: _items[position].time,
                      address: _items[position].address!,
                      rating: _items[position].rating!,
                      category: _items[position].category!,
                      image: _items[position].image,
                      onTap: () => UiUtils.goToWidget(CartScreen(
                            item: _items[position],
                          )));
                },
                separatorBuilder: (context, position) {
                  return SizedBox(
                    height: kSpacingMedium,
                  );
                },
                itemCount: _items.length)
            .paddingSymmetric(horizontal: kSpacingMedium));
  }
}
