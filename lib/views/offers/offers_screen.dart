import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/offer.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/offers/offers_vm.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_vm.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/category_single_widget/special_offers.dart';
import 'package:provider/provider.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/home_widgets/order_again.dart';
import 'package:grigora/views/cart/cart_screen.dart';
import 'package:grigora/ui_widgets/rating_widget.dart';
import 'package:grigora/ui_widgets/grigora_tab_widget.dart';

class OffersScreen extends StatelessWidget {
  const OffersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return LayoutWithBack(
      title: 'Offers',
      withStack: false,
      withShoppingCart: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FilterWidget()
                .paddingTop(kSpacingSmall)
                .paddingSymmetric(horizontal: kSpacingMedium),
            OfferWidget().paddingTop(kSpacingLarge),
          ],
        ),
      ),
    );
  }
}

class OfferWidget extends StatelessWidget {
  const OfferWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<OffersWidgetModel> _items = [
      OffersWidgetModel(
          title: 'Burgers',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1606739212407-ee40ccdec7f8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      OffersWidgetModel(
          title: 'Pizzas',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1573555957315-723d970bcdde?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Zm9vZCUyMHZlbmRvcnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      OffersWidgetModel(
          title: 'Chicken',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1572695064956-52bfdd19cfee?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
      OffersWidgetModel(
          title: 'Drinks',
          time: '32 Mins',
          address: 'No5, st Gogery Rd. beside G',
          rating: 4.8,
          category: 'Fast Food',
          image:
              'https://images.unsplash.com/photo-1422919869950-5fdedb27cde8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGZvb2QlMjB2ZW5kb3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=600&q=60'),
    ];

    return GrigoraSectionWidget(
        title: '',
        hideTitle: true,
        showAllAction: () => print('go somewhere'),
        child: ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, position) {
                  return OfferItem(
                      title: _items[position].title,
                      time: _items[position].time,
                      offer: _items[position].discount,
                      rating: _items[position].rating,
                      minOrder: _items[position].minOrder,
                      image: _items[position].image,
                      onTap: () {});
                },
                separatorBuilder: (context, position) {
                  return SizedBox(
                    height: kSpacingMedium,
                  );
                },
                itemCount: _items.length)
            .paddingSymmetric(horizontal: kSpacingMedium));
  }
}
