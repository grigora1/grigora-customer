import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/category_single_widget/categories_widget.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/category_single_widget/food_items.dart';
import 'package:grigora/ui_widgets/category_single_widget/food_vendor_nearby.dart';
import 'package:grigora/ui_widgets/category_single_widget/popular_cuisines.dart';
import 'package:grigora/ui_widgets/category_single_widget/popular_restaurants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/views/restaurants/restaurants_vm.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/home_widgets/purchase_an_item.dart';
import 'package:grigora/ui_widgets/home_widgets/what_would_you_like_to_do.dart';
import 'package:grigora/ui_widgets/home_widgets/order_again.dart';
import 'package:grigora/ui_widgets/home_widgets/top_orders_today.dart';
import 'package:grigora/ui_widgets/home_widgets/popular_near_you.dart';
import 'package:grigora/ui_widgets/home_widgets/slider_widget.dart';
import 'package:grigora/ui_widgets/home_widgets/favourite_restaurant.dart';
import 'package:grigora/ui_widgets/home_widgets/special_offers.dart';
import 'package:grigora/ui_widgets/home_widgets/recommended_vendors.dart';

class RestaurantsScreen extends StatefulWidget {
  const RestaurantsScreen({Key? key}) : super(key: key);

  @override
  _RestaurantsScreenState createState() => _RestaurantsScreenState();
}

class _RestaurantsScreenState extends State<RestaurantsScreen> {
  int _lastSelected = 0;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RestaurantsVM>(
        model: RestaurantsVM(),
        builder: (context, model, child) {
          return Scaffold(
            appBar: new AppBar(
              automaticallyImplyLeading: false,
              title: _AppBarTitle(),
              centerTitle: true,
              elevation: 0,
              backgroundColor: kColorWhite,
            ),
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SearchWidget().paddingAll(kSpacingMedium),
                  SliderWidget().paddingBottom(kSpacingLarge),
                  CategoriesWidget().paddingBottom(kSpacingLarge),
                  FilterWidget()
                      .paddingBottom(kSpacingLarge)
                      .paddingSymmetric(horizontal: kSpacingMedium),
                  PopularRestaurants().paddingBottom(kSpacingLarge),
                  WhatWouldYouLikeToDo().paddingBottom(kSpacingLarge),
                  OrderAgain().paddingBottom(kSpacingMedium),
                  FoodVendorNearby().paddingBottom(kSpacingLarge),
                  PopularCuisines().paddingBottom(kSpacingLarge),
                  SpecialOffers().paddingBottom(kSpacingLarge),
                  FoodItems().paddingBottom(kSpacingLarge),
                ],
              ),
            ),
          );
        });
  }
}

class _AppBarTitle extends StatelessWidget {
  const _AppBarTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(kImagesIconLocation).paddingRight(kPaddingSmall),
        Expanded(
            child: Row(
          children: [
            Expanded(
                child: Row(
              children: [
                Expanded(
                    child: GrigoraTextTitle(
                  'Qrelab Office',
                  isLightFont: true,
                )),
                Icon(
                  EvaIcons.chevronDown,
                  color: kColorBlack,
                  size: 30,
                ).paddingRight(kPaddingSmall)
              ],
            )),
            SvgPicture.asset(kImagesIconCart),
          ],
        )),
      ],
    );
  }
}
