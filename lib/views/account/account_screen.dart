import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/category_single_widget/filter_widget.dart';
import 'package:grigora/ui_widgets/grigora_horizontal_scroll.dart';
import 'package:grigora/ui_widgets/grigora_network_image.dart';
import 'package:grigora/ui_widgets/offer.dart';
import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/ui_widgets/vendor.dart';
import 'package:grigora/utils/base_widget.dart';
import 'package:grigora/ui_widgets/search_widget.dart';
import 'package:grigora/utils/ui_helper.dart';
import 'package:grigora/views/edit_profile/edit_profile_screen.dart';
import 'package:grigora/views/home/filter_screen.dart';
import 'package:grigora/views/offers/offers_vm.dart';
import 'package:grigora/views/payment_methods/payment_methods_screen.dart';
import 'package:grigora/views/restaurant_single/restaurant_single_vm.dart';
import 'package:grigora/ui_widgets/grigora_section_widget.dart';
import 'package:grigora/views/saved_address/saved_address.dart';
import 'package:grigora/views/settings/settings_screen.dart';
import 'package:grigora/views/subscription/subscription_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/category_single_widget/special_offers.dart';
import 'package:provider/provider.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';
import 'package:grigora/ui_widgets/home_widgets/order_again.dart';
import 'package:grigora/views/cart/cart_screen.dart';
import 'package:grigora/ui_widgets/rating_widget.dart';
import 'package:grigora/ui_widgets/grigora_tab_widget.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return LayoutWithBack(
      title: 'Account',
      withStack: false,
      withShoppingCart: false,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(
                  EditProfileScreen(),
                );
              },
              title: 'Profile',
              subtitle: 'Natty Nate',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {},
              title: 'My Wishlist',
              subtitle: '6 items in wishlist',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(SavedAddressScreen());
              },
              title: 'Address',
              subtitle: '2 locations',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(PaymentMethodsScreen());
              },
              title: 'Payment details',
              subtitle: '2 cards saved',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(SubscriptionScreen());
              },
              title: 'Dzooco free Delivery subscription',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TextAndChevron(
              onTap: () {},
              title: 'Refer friends, Get N500',
              color: kColorPrimaryRed,
              big: false,
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TitleSubButton(
              onTap: () {
                UiUtils.goToWidget(SettingsScreen());
              },
              title: 'Settings',
              subtitle: 'search, orders, Push notification & SMS',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingLarge),
            _TextAndChevron(
              onTap: () {},
              title: 'Legal',
              big: true,
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingMedium),
            _TextAndChevron(
              onTap: () {},
              title: 'Become a Postman',
              color: kColorPrimaryRed,
              big: false,
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingLarge),
            _TitleSubButton(
              onTap: () {},
              title: 'Log Out',
            )
                .paddingSymmetric(horizontal: kSpacingMedium)
                .paddingTop(kSpacingLarge)
                .paddingBottom(kSpacingLarge),
          ],
        ),
      ),
    );
  }
}

class _TitleSubButton extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function() onTap;
  const _TitleSubButton(
      {Key? key, this.title = '', this.subtitle = '', required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GrigoraTextTitle(
                  title,
                  textAlign: TextAlign.start,
                  textSize: 18,
                ),
                Visibility(
                  visible: this.subtitle != '',
                  child: GrigoraText(
                    subtitle,
                    textColor: kColorGenericGrey,
                    textAlign: TextAlign.start,
                    textSize: 14,
                  ).paddingTop(kSpacingSmall),
                )
              ],
            ).paddingLeft(kSpacingMedium),
            Container(
              height: 0.2,
              decoration: BoxDecoration(color: kColorGenericGrey),
            ).paddingTop(kSpacingMedium).paddingTop(kSpacingSmall)
          ],
        ).paddingTop(kSpacingSmall),
      ),
    );
  }
}

class _TextAndChevron extends StatelessWidget {
  final String title;
  final Color color;
  final bool big;
  final Function() onTap;
  const _TextAndChevron(
      {Key? key,
      this.title = '',
      this.color = Colors.black,
      this.big = true,
      required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GrigoraTextTitle(
              title,
              textAlign: TextAlign.start,
              textSize: big ? 18 : 16,
              textColor: color,
              isLightFont: !big,
            ),
            Icon(
              Icons.chevron_right,
              color: color,
              size: 18,
            )
          ],
        ).paddingTop(kSpacingSmall).paddingLeft(kSpacingMedium),
      ),
    );
  }
}
