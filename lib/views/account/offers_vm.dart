import 'package:grigora/ui_widgets/restaurants.dart';
import 'package:grigora/utils/base_model.dart';

class OffersVM extends BaseModel {
  final OffersWidgetModel item;
  OffersVM(this.item);
}
