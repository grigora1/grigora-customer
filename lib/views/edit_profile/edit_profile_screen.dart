import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/buttons.dart';
import 'package:grigora/ui_widgets/text.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/utils/form_item.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:grigora/ui_widgets/layouts/layout_with_back.dart';

class EditProfileScreen extends StatelessWidget {
  const EditProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutWithBack(
      title: 'Profile',
      withShoppingCart: false,
      body: Column(
        children: [_FormWidget()],
      ).paddingAll(kSpacingMedium),
    );
  }
}

class _FormWidget extends StatelessWidget {
  const _FormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          FormItem(
            placeholder: 'First Name',
            label: 'First Name',
          ),
          FormItem(
            placeholder: 'Last Name',
            label: 'Last Name',
          ).paddingTop(kSpacingMedium),
          FormItem(
            placeholder: '+234 80...',
            label: 'Phone Number',
          ).paddingTop(kSpacingMedium),
          FormItem(
            placeholder: 'Email address',
            label: 'Email',
          ).paddingTop(kSpacingMedium),
          GrigoraButton(
            onTap: () => UiUtils.goBack(),
            text: 'Save Changes',
            withBg: true,
          ).paddingTop(kSpacingLarge)
        ],
      ),
    );
  }
}
