import 'package:custom_navigator/custom_navigator.dart';
import 'package:flutter/material.dart';
import 'package:grigora/constants.dart';
import 'package:grigora/ui_widgets/home_widgets/fab_bottom_app_bar.dart';
import 'package:grigora/ui_widgets/ui_utils.dart';
import 'package:grigora/views/account/account_screen.dart';
import 'package:grigora/views/offers/offers_screen.dart';
import 'package:grigora/views/orders/orders_screen.dart';
import 'package:grigora/views/restaurants/restaurants_screen.dart';
import 'package:grigora/views/search/search_screen.dart';
import 'package:grigora/views/track_order/track_order_screen.dart';

class BottomBarPage extends StatefulWidget {
  const BottomBarPage({Key? key}) : super(key: key);

  @override
  _BottomBarPageState createState() => _BottomBarPageState();
}

class _BottomBarPageState extends State<BottomBarPage> {
  int _lastSelected = 0;
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  void _selectedTab(int index) {
    navigatorKey.currentState?.maybePop();
    setState(() {
      _lastSelected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      bottomNavigationBar: FABBottomAppBar(
        selectedColor: kColorPrimaryRed,
        onTabSelected: _selectedTab,
        iconSize: width * 0.06,
        backgroundColor: Colors.white,
        centerItemText: '',
        color: Colors.white,
        height: height * 0.055,
        items: [
          FABBottomAppBarItem(
            imageAsset: kImagesMarketIcon,
            title: 'Market'
          ),
          FABBottomAppBarItem(
            imageAsset: kImagesSearchIcon,
            title: 'Search'
          ),
          FABBottomAppBarItem(
            imageAsset: kImagesOffersIcon,
            title: 'Offers'
          ),
          FABBottomAppBarItem(
            imageAsset: kImagesOrdersIcon,
            title: 'Orders'
          ),
          FABBottomAppBarItem(
            imageAsset: kImagesAccountIcon,
            title: 'Account'
          ),
        ],
      ),
      body: CustomNavigator(
        navigatorKey: navigatorKey,
        pageRoute: PageRoutes.materialPageRoute,
        home: _lastSelected == 0
            ? RestaurantsScreen()
            : _lastSelected == 1
                ? SearchScreen()
                : _lastSelected == 3
                    ? TrackOrderScreen()
                    : _lastSelected == 2
                        ? OffersScreen()
                        : AccountScreen(),
      ),
    );
  }
}
